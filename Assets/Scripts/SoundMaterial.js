var behave:String = "";
var oscId:int = 0;
var minNum:Number = .4;
var multiplier:Number = 4;
var scaleMax = 3.0;
public var behaveId:int = 0;
private var curVal : Number = 0.0;
private var origColor : Color;
private var origScale : Number = 0.1;
private var likelihood:int = 5;
public var scaleMin = 0.1;
private var colorMode:int = 0;

function setMode(n){
	colorMode = n;
	updateColor();
}

function updateColor(){
	origColor = renderer.material.color = ColorSchemes.GetByMode(colorMode);
}



function OnCollisionEnter( )
{
	renderer.material.color = Color.white;
}

function OnCollisionExit( )
{
	renderer.material.color = origColor;
}


function Start(){
	updateColor();	
	origScale = Random.Range(scaleMin,scaleMax);
	transform.localScale = Vector3(origScale,origScale,origScale);

	switch(behave){
		case "blink": behaveId = 1; break;
		case "scale": behaveId = 2; break;
		case "rotate": behaveId = 3; break;
	}	
}

function seeMe(){
	return renderer && renderer.isVisible;
}

function Update () {
	if(!seeMe()) return;
	
	curVal = OSCReceiver.messages[oscId] * multiplier + minNum;
	switch(behaveId){
		case 1: blink(); break;
		case 2: scale(); break;
	}
	rotate();
}


function rotate(){
	transform.Rotate(Vector3(OSCReceiver.messages[0],OSCReceiver.messages[1],OSCReceiver.messages[2])*multiplier);
}

function blink(){
	renderer.material.color =  origColor * curVal;
}

function scale(){
	transform.localScale = Vector3(curVal,curVal,curVal) * origScale;
}