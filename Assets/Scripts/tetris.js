var mat:Material;
var mat2:Material;
var delay:float;
var repeat:int;
var cam : Camera;
var targ : GameObject;
var targ2 : GameObject;
var targ3 : GameObject;
var targ4 : GameObject;
var maxCount : int = 50;
private var cubeAr:Array = [];
var tick : int = 0;
function Start(){

}		

function Update(){
	if(cubeAr.length < maxCount && tick == 30){
	//	spawnCube();
		spawnTarget();
		tick = 0;
	}
	tick++;
	
	
	if(cubeAr.length > 0 && Mathf.Floor(Random.value*200) == 10){
		var script : CamLook = cam.GetComponent("CamLook");
		var obj = cubeAr[Mathf.Floor(cubeAr.length * Random.value)];
		script.SetTarget(obj.transform);
	}
	cam.fieldOfView += .005;
}

function spawnTarget(){
	var cases = [targ,targ2,targ3,targ4];
	
	var t = cases[Mathf.Floor(Random.value * cases.length)];
	var renderers;
	renderers = t.GetComponentsInChildren(Renderer);
	for (var r in renderers) {
	    r.material = mat;
	}

	var d = 1;
	
	var obj = Instantiate(t,Vector3(Mathf.Floor(Random.value*10)-5,10 + cubeAr.length*2,0), Quaternion.identity);
	obj.AddComponent(Rigidbody);
	cubeAr.push(obj);
	obj.rigidbody.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationY;
	obj.rigidbody.AddForce(Vector3(0,-10,0), ForceMode.Impulse);
	
//	cam.transform.LookAt(obj.transform);

	// script.target = obj.transform;
}
