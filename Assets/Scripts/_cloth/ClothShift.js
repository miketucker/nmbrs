private var amp:Number = 5;
private var sp:Vector3;
function Start(){
	sp = transform.position;
}

function Update () {
	rigidbody.velocity += (transform.position - sp) * -.1;
	if(!OSCReceiver) return;
	if(OSCReceiver.messages[0] < .5) return;
	var amt:Number = amp * OSCReceiver.messages[0];
	rigidbody.velocity += Vector3(Random.Range(-amt,amt),Random.Range(-amt,amt),Random.Range(-amt,amt));
}