var obj:Material;
var obj2:Material;
var delay:float;
var repeat:int;
var cam : Camera;
var targ : GameObject;
var targ2 : GameObject;
var targ3 : GameObject;
var targ4 : GameObject;
var maxCount : int = 50;
private var cubeAr:Array = [];

function Start(){

}		

function Update(){
	if(cubeAr.length < maxCount && Mathf.Floor(Random.value* 50) == 5){
	//	spawnCube();
		spawnTarget();
	}
	
	if(cubeAr.length > 0 && Mathf.Floor(Random.value*50) == 10){
		var cube = cubeAr[Mathf.Floor(cubeAr.length * Random.value)];
		cube.rigidbody.AddForceAtPosition(Vector3(0,Random.value*10,0), Vector3(Random.value*10,Random.value*10,Random.value*10), ForceMode.Impulse);
		var s = Random.value*1.5;
		iTween.ScaleTo(cube,{"amount": Vector3(s,s,s), "time":1.6});
	}
}

function spawnTarget(){
	var cases = [targ,targ2,targ3,targ4];
	
	var t = cases[Mathf.Floor(Random.value * cases.length)];
	var d = 1;
	
	var obj = Instantiate(t,Vector3(Random.value * d - d/2,Random.value * 5 + 1,Random.value * d - d/2), Quaternion.identity);
	obj.AddComponent(Rigidbody);
	var s1 = Random.value*1;
	obj.transform.localScale = Vector3(s1,s1,s1);
	cubeAr.push(obj);
}

function spawnCube(){
	
	var cube = GameObject.CreatePrimitive(PrimitiveType.Sphere);
	cube.AddComponent(Rigidbody);
	cube.renderer.material = obj;
	//cube.transform.localScale = Vector3(0,0,0);
	//iTween.PunchScale(cube,{"amount": Vector3(1,1,1), "time":1.6});
	
	var r = 4;
	
	cube.transform.position = Vector3(Random.value * r - r/2,Random.value * r - r/2,Random.value * 5);
	cubeAr.push(cube);
	//var script : CamFollow = cam.GetComponent("CamFollow");
	//script.target = cube.transform;
	
	// iTween.PunchScale(cube,{"amount": Vector3(2,2,2), "time":1.6});
	
}