
var amp:Number = 100;
var anim:ParticleAnimator;
function Start(){
	anim = GetComponent("ParticleAnimator");
}


function Update () {
	if(!OSCReceiver || !OSCReceiver.messages[0]) return;
	if(OSCReceiver.messages[0] > 1 - KeyboardManager.strobeLevel){
		anim.rndForce = Vector3(amp,amp,amp);
	} else {
		anim.rndForce = Vector3.one;
	}
}