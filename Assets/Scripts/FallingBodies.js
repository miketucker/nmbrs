var targ : GameObject;

private var objs : Array = [];
private var tick : int = 0;
private var maxCount = 50;
private var d:Number = 5;
private var delay:Number = 20;
function Update () {
	
	if(objs.length>maxCount){
		
		var o = objs.shift();
		Debug.Log(o);
		//o.Destroy(Rigidbody);
		//iTween.ScaleTo(o,{"amount": Vector3(0.01,0.01,0.01), "time":3.0});
		 remove(o);
	}
	
	if( tick == delay){
		spawnTarget();
		tick = 0;
	} 
	tick++;
	
}

function remove(o){
	yield WaitForSeconds(1.0);
	Destroy(o);
}


function spawnTarget(){
	var q:Quaternion = new Quaternion();
	q.x = Random.value;
	q.y = Random.value;
	q.z = Random.value;
	
	var obj = Instantiate(targ,Vector3(Random.Range(-d,d),Random.Range(10,10),Random.Range(-d,d)),q);
	obj.AddComponent(Rigidbody);
			

	objs.push(obj);
}
