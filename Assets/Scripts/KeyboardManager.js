var cameras:Camera[];
private var camId:int = 0;
public var blackTexture:Texture2D;
private var startTime:Number;
public static var fadeLevel:Number = 0;
public static var shakeLevel:Number = 0;
public static var shakeIntensity:Number = 0;
public static var strobeLevel:Number = 0;
public static var spinMultiplier:Number = 1;
public static var fov:float = 45;
public static var firstBoot:boolean = true;
private var jumping:int = 0;
function OnGUI() {
    var e : Event = Event.current;
    if (e.isKey && e.type == EventType.KeyDown) {
        keyEvent(e);
    }


    if(fadeLevel > 0){ 
		GUI.color = new Color(0,0,0, fadeLevel * 2 - .5);
		GUI.DrawTexture( new Rect(0, 0, Screen.width, Screen.height ), blackTexture );
	}



}

function getCam(){
	return cameras[camId];
}

function Awake(){
	if(firstBoot){
		// Screen.SetResolution(2048,768,true);
		firstBoot = false;
	}
	Screen.showCursor = false;
}

function Start(){
	// Screen.SetResolution(1280,480,false);
	// Screen.fullScreen = true;
	changeCamera(0);
}

function LateUpdate(){
	if(OSCReceiver && OSCReceiver.messages[0] > 1 - shakeLevel) shake();
	// if(OSCReceiver && OSCReceiver.messages[0] > 1 - strobeLevel) strobe();
	cameras[camId].backgroundColor = (OSCReceiver && OSCReceiver.messages[0] > 1 - strobeLevel) ? Color.white : Color.black;
	cameras[camId].fieldOfView += (fov - cameras[camId].fieldOfView) * .1;
}

function JumpCam(){
	if(jumping > 0) return;
	jumping = 20;
	var vec:Vector3 = cameras[camId].transform.position;
	while(jumping > 0){
	// if(Random.value > .5){
		var v:Vector3 = cameras[camId].transform.forward * jumping * .1 * shakeIntensity;
		if(cameras[camId].GetComponent("MaxCamera")) cameras[camId].GetComponent("MaxCamera").position += v;
		else cameras[camId].transform.position += v;
		jumping --;
		yield WaitForSeconds(.01);
	}
	cameras[camId].transform.position = vec;
}


function shake(){
	if(Random.value > .5){
		var v:Vector3 = Vector3(Random.value-.5,Random.value-.5,Random.value-.5) * shakeIntensity  * OSCReceiver.messages[0];
		if(cameras[camId].GetComponent("MaxCamera")) cameras[camId].GetComponent("MaxCamera").position += v;
		else cameras[camId].transform.position += v;
	}
}

function keyEvent(e:Event){
	switch(e.keyCode){
		case KeyCode.Alpha1: changeScene(1); break;
		case KeyCode.Alpha2: changeScene(2); break;
		case KeyCode.Alpha3: changeScene(3); break;
		case KeyCode.Alpha4: changeScene(4); break;
		case KeyCode.Alpha5: changeScene(5); break;
		case KeyCode.Alpha6: changeScene(6); break;
		case KeyCode.Alpha7: changeScene(7); break;
		case KeyCode.Alpha8: changeScene(8); break;
		case KeyCode.Alpha9: changeScene(9); break;
		case KeyCode.Alpha0: changeScene(10); break;
		case KeyCode.Minus: changeScene(11); break;
		case KeyCode.Z: changeScene(12); break;
		case KeyCode.X: changeScene(13); break;
		case KeyCode.C: changeScene(14); break;
		case KeyCode.V: changeScene(15); break;
		case KeyCode.A: changeScene(16); break;
		case KeyCode.Period: changeCamera(1); break;
		case KeyCode.Comma: changeCamera(-1); break;
		case KeyCode.RightBracket: Screen.showCursor = !Screen.showCursor;
	}
}

function changeCamera(n){
	for(var c:Camera in cameras)
		c.enabled = false;
	camId = Mathf.Clamp(camId+n,0,cameras.length -1);
	fov = cameras[camId].fieldOfView;
	cameras[camId].enabled = true;
}

function changeScene(n){
	Application.LoadLevel(n-1);	
}