private var jiggleCount:int = 0;
private var rigid:Rigidbody;
var swapMaterial : Material;
private var origMaterial : Material;

function Start(){
	origMaterial = renderer.material;
	rigid = GetComponent("Rigidbody");
}


function Update(){
	rigid.velocity += (Vector3.zero - transform.position)* .9;	
}


function OnCollisionEnter( )
{
	renderer.material = swapMaterial;
}

function OnCollisionExit(){
	resetMaterial();
}

function resetMaterial(){
	WaitForSeconds(10);
	Debug.Log('reset material');
	renderer.material = origMaterial;
}

function jiggle(){
	jiggleCount++;
	transform.Rotate(Random.Range(-5,5),Random.Range(-5,5),Random.Range(-5,5));
	
	if(jiggleCount < 50){
		doAnotherJiggle();
	} else {
		endJiggle();
	}
}

function doAnotherJiggle(){
	WaitForSeconds(.1);
	jiggle();	
}

function endJiggle(){
	jiggleCount = 0;
	transform.localRotation = Quaternion.identity;
}