var dupe: GameObject;
private var objAr:Array = [];
private var origAr:Array = [];

private var FieldPosition : Vector3;
var FieldRadius : float;
var FieldForce : float;
var targetAr : Array = [];
private var matAr:Array = [Color(.1,0.7,1.0,1.0),Color(1.0,0,.5,1.0),Color(.7,.5,.1,1.0)];

private var centerPull:Number = 0.003;

function getTarget(){
	return targetAr[0];
}



function Start(){
	var r = 8;
	var c = 8;
	var d = 5;
	var s = 4;
	var off = s * (r-1) * .5;
	
	Physics.gravity = Vector3(0, 0, 0);
	
	for(var i = 0; i < r ; i++){
		for(var j = 0; j < c ; j++){
			for(var k = 0; k < d; k++){
				var obj = Instantiate(dupe,Vector3(i * s - off, k * s, j * s - off), Quaternion.identity);
				objAr.push(obj);
				var s1 = Random.value*3;
				obj.transform.localScale = Vector3(s1,s1,s1);
				origAr.push(Vector3(i * s - off, k * s, j * s - off));
//				obj.AddComponent(Rigidbody);
				obj.renderer.material.color = matAr[Mathf.Floor(Random.Range(0,3))];
				if(i==5) targetAr.push(obj);
			}
		}
	}
	
	
	
	
}