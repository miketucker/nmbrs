public var groundObj:GameObject;

var logoAr:GameObject[];
var objAr:GameObject[];
var techAr:GameObject[];
var pillAr:GameObject[];
var maxObjectCount : int = 50;
var waitTime : int = 10;
private var wait : int =  0;
private var objs:Array = [];
private var playMode : int = 0;
function Start(){
		
}

function Update () {
	if(objs.length > maxObjectCount){	
		Destroy(objs.shift());
	}

	groundObj.transform.localScale.y = 100 + OSCReceiver.messages[0] * 10;

	if(wait == 0){
		spawnObject();
		wait = waitTime;
		if(playMode == 4) wait *= 3;
	} else {
		wait --;	
	}
}

function spawnObject(){
	var q = new Quaternion();
	q.x = Random.value;
	q.y = Random.value;
	q.z = Random.value;
	
	var obj;
	switch(playMode){
		case 6: obj = logoAr[Random.Range(0,logoAr.length)]; break;
		case 4:
			obj = techAr[Random.Range(0,techAr.length)];
			break;
		case 5:
			obj = pillAr[Random.Range(0,pillAr.length)];
			break;
		default:
			obj = objAr[Random.Range(0,objAr.length)];
			break;
	}
	var o = Instantiate(obj,Vector3(Random.Range(-1,1),Random.Range(20,30),0),q);
	if(playMode < 4) o.GetComponent('SoundMaterial').setMode(playMode);
	else {
		var s;
		if(playMode == 5) s = Random.Range(0.7,2.0);
		else if(playMode == 6) s = Random.Range(0.5,3.0);
		else s = Random.Range(0.4,1.0);
		o.transform.localScale = Vector3(s,s,s);
		if(playMode == 4 || playMode==6){
		o.AddComponent("MeshCollider");	
		o.GetComponent("MeshCollider").convex = true;
		}
		o.AddComponent("Rigidbody");	
		o.GetComponent("Rigidbody").collisionDetectionMode = CollisionDetectionMode.Continuous;
	}
	
	objs.push(o);
}


function setMode(n){
	playMode = n;
	if(n < 4){
		for(var i = 0; i < objs.length; i++){
			WaitForSeconds(1);
			objs[i].GetComponent('SoundMaterial').setMode(n);	
		}	
	} else {
		clearAll();	
	}
}

function clearAll(){
	while(objs.length > 0)
		Destroy(objs.pop());
}


function OnGUI(){
	var e : Event = Event.current;
    if (e.isKey && e.type == EventType.KeyDown) {
        keyEvent(e);
    }
}

function keyEvent(e:Event){
	switch(e.keyCode){
		case KeyCode.L: setMode(6); break;
		case KeyCode.K: setMode(5); break;
		case KeyCode.J: setMode(4); break;
		case KeyCode.V: setMode(0); break;
		case KeyCode.B: setMode(1); break;
		case KeyCode.N: setMode(2); break;	
		case KeyCode.M: setMode(3); break;	
	}
}