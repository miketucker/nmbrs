var objAr:GameObject[];
var maxObjectCount : int = 50;
var waitTime : int = 10;
private var wait : int =  0;
private var objs:Array = [];
private var playMode : int = 0;
private var rad:Number = 6;
private var torque:Number = .1;
function Start(){
	SpawnLoop();
}

function SpawnLoop(){
	while(true){
		if(objs.length > maxObjectCount){	
			Destroy(objs.shift());
		}
		spawnObject();

		yield WaitForSeconds(Random.Range(2,3));
	}
}


function spawnObject(){

	
	var obj;
	
	obj = objAr[Random.Range(0,objAr.length)];
	var o = Instantiate(obj,Vector3(Random.Range(-rad,rad),Random.Range(15,30),Random.Range(-rad,rad)),Quaternion.identity);
	o.transform.Rotate(Random.Range(-90,90),Random.Range(-90,90),Random.Range(-90,90));
		var s;
		s = Random.Range(0.3,3);
		o.transform.localScale = Vector3(s,s,s);
		o.AddComponent("MeshCollider");	
		o.GetComponent("MeshCollider").convex = true;
		o.AddComponent("Rigidbody");	
		o.GetComponent("Rigidbody").collisionDetectionMode = CollisionDetectionMode.Continuous;
		o.rigidbody.mass = .0001;
		o.rigidbody.drag = 5;
		// o.rigidbody.AddRelativeTorque(Random.Range(-torque,torque),Random.Range(-torque,torque),Random.Range(-torque,torque));
		o.rigidbody.angularDrag = .1;

	objs.push(o);
}


function setMode(n){
	playMode = n;
	if(n < 4){
		for(var i = 0; i < objs.length; i++){
			WaitForSeconds(1);
			objs[i].GetComponent('SoundMaterial').setMode(n);	
		}	
	} else {
		clearAll();	
	}
}

function clearAll(){
	while(objs.length > 0)
		Destroy(objs.pop());
}


function OnGUI(){
	var e : Event = Event.current;
    if (e.isKey && e.type == EventType.KeyDown) {
        keyEvent(e);
    }
}

function keyEvent(e:Event){
	switch(e.keyCode){
		case KeyCode.L: setMode(6); break;
		case KeyCode.K: setMode(5); break;
		case KeyCode.J: setMode(4); break;
		case KeyCode.V: setMode(0); break;
		case KeyCode.B: setMode(1); break;
		case KeyCode.N: setMode(2); break;	
		case KeyCode.M: setMode(3); break;	
	}
}