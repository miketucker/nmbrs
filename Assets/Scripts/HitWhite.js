private var origColor:Color;

function Start(){
	origColor = renderer.material.color;
}

function OnCollisionEnter( )
{
	renderer.material.color = Color.white;
}

function OnCollisionExit( )
{
	renderer.material.color = origColor;
}
