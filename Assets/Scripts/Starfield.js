private var objs:Array = [];
public var ballCount:int = 100;
public var dist:Number = 10;
public var distY:Number = 0;
public var dupe:GameObject;
public var ballSize:Number = .05;
public var minBallSize:Number = 0;
public var matchSound:boolean = false;
public var blink:boolean = false;
public var mode:int = 0;


function Start(){
	if(minBallSize==0) minBallSize = ballSize;
	if(distY==0) distY = dist;
	for (var i = 0; i < ballCount; i++){
		d = Random.Range(1,dist);
		var p = Random.Range(-3.16,3.16);
		
		var _x = Mathf.Cos(p) * d;
		var _z = Mathf.Sin(p) * d;
		
		var obj:GameObject = Instantiate(dupe);
		obj.transform.parent = transform;
		obj.transform.localPosition = Vector3(_x,Random.Range(-distY,distY),_z);
		if(!matchSound){ 
			obj.GetComponent("StarBlinkBackup").enabled = false;
			obj.renderer.material.color = Color.white;
		} else {
			if(blink) obj.GetComponent("StarBlinkBackup").doBool = true;
			obj.GetComponent("StarBlinkBackup").setMode(mode);

		}
		obj.transform.localScale = Vector3.one * Random.Range(minBallSize,ballSize);
		objs.push(obj);
	}	
	setMode(5);
}

function setMode(n:int){
	mode = n;
	var ar = GameObject.FindGameObjectsWithTag("Star");

	for(var g:GameObject in ar){
		if(g.GetComponent("StarBlinkBackup")) g.GetComponent("StarBlinkBackup").setMode(n);
		else return;
	}
}