/*
This camera smoothes out rotation around the y-axis and height.
Horizontal Distance to the target is always fixed.

There are many different ways to smooth the rotation but doing it this way gives you a lot of control over how the camera behaves.

For every of those smoothed values we calculate the wanted value and the current value.
Then we smooth it using the Lerp function.
Then we apply the smoothed values to the transform's position.
*/

// The target we are following
var target : Transform;
// The distance in the x-z plane to the target
var distance = 10.0;
// the height we want the camera to be above the target
var height = 5.0;
// How much we 
var heightDamping = 2.0;
var rotationDamping = 3.0;
var duration = 100000;
// Place the script in the Camera-Control group in the component menu
@script AddComponentMenu("Camera-Control/Smooth Follow")
var startTime = 0;
var curRot;

function SetTarget(targ){
	curRot = transform;
	
	Debug.Log( Time.time - startTime);
	
	startTime = Time.time;
	
	
	var t = new GameObject().transform;
	t.transform.position = transform.position;
	t.LookAt(targ);
	
	target = t;
}

function LateUpdate () {
    // Early out if we don't have a target
    if (!target)
        return;
	
	    transform.rotation = Quaternion.Slerp (curRot.rotation, target.rotation, (Time.time - startTime) / duration);

}