
//You can set these variables in the scene because they are public 
public var RemoteIP : String = "127.0.0.1";
public var SendToPort : int = 57131;
public var ListenerPort : int = 57130;
public var controller : Transform; 
private var handler : Osc;
static var messages : float[] = [0.0,0.0,0.0,0.0,0.0];

private var data:Array = [];

public function Start ()
{
	//Initializes on start up to listen for messages
	//make sure this game object has both UDPPackIO and OSC script attached
	

	var udp : UDPPacketIO = GetComponent("UDPPacketIO");
	udp.init(RemoteIP, SendToPort, ListenerPort);
	handler = GetComponent("Osc");
	handler.init(udp);
			
	handler.SetAddressHandler("/VDMX/1", ListenEvent);
	handler.SetAddressHandler("/VDMX/2", ListenEvent);
	handler.SetAddressHandler("/VDMX/3", ListenEvent);
	handler.SetAddressHandler("/VDMX/4", ListenEvent);
	handler.SetAddressHandler("/VDMX/5", ListenEvent);
	handler.SetAddressHandler("/VDMX/6", ListenEvent);
	handler.SetAddressHandler("/VDMX/7", ListenEvent);
	handler.SetAddressHandler("/VDMX/8", ListenEvent);
	handler.SetAddressHandler("/VDMX/9", ListenEvent);
	handler.SetAddressHandler("/VDMX/10", ListenEvent);

}

public function OnGUI()
{
	if(Input.GetKeyDown(KeyCode.Space)){
		Debug.Log(data.Join(","));
	}
}

public function FixedUpdate()
{
	data.push(Mathf.Round(messages[0]*10),Mathf.Round(messages[1]*10),Mathf.Round(messages[2]*10),Mathf.Round(messages[3]*10));
}

static public function GetLevel(percent:float) : float {
	percent *= 4;
	var start:int = Mathf.Floor(percent);
	var f:float = Mathf.Lerp(messages[start],messages[start+1],percent - start);

	return f;
}


public function ListenEvent(oscMessage : OscMessage) : void
{	
	var i:int = 0;



	switch(oscMessage.Address){
		case "/VDMX/1":	i = 0; break;
		case "/VDMX/2":	i = 1; break;
		case "/VDMX/3":	i = 2; break;
		case "/VDMX/4":	i = 3; break;
		case "/VDMX/5":	KeyboardManager.fadeLevel = oscMessage.Values[0]; return;;
		case "/VDMX/6":	KeyboardManager.strobeLevel = oscMessage.Values[0]; return;
		case "/VDMX/7":	KeyboardManager.shakeLevel = oscMessage.Values[0]; return;
		case "/VDMX/8":	KeyboardManager.shakeIntensity = oscMessage.Values[0]; return;
		case "/VDMX/9":	KeyboardManager.spinMultiplier = 1 + 8 * oscMessage.Values[0]; return;
		case "/VDMX/10": 
			var n:float = 1 + 160 * oscMessage.Values[0];
			KeyboardManager.fov = n; 

			return;
		
	}
	messages[i] = 1.0 * oscMessage.Values[0];
} 
