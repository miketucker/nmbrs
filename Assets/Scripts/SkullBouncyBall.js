var multiplier : Number = 2.0;

private var origColor:Color = new Color();
private var rigid:Rigidbody;
private var matAr:Array = [Color(0.5,1.0,0.5,1.0),Color(1.0,0.0,.5,1.0),Color(0.0,1.0,0.5,1.0),Color(1.0,0.0,1.0,1.0)];
private var oscId : int = 0;
private var curVal : Number = 0.0;
private var origScale : Number = 0.1;
private var v : Number = 20.0;
private var colorMode : int = 0;
function Start(){
	origScale = Random.Range(.01,1.0);
	transform.localScale = Vector3(origScale,origScale,origScale);
	
	oscId = Random.Range(0,matAr.length);
	updateColor();
	rigid = GetComponent("Rigidbody");
}



function setMode(n){
	colorMode = n;
	updateColor();
}


function updateColor(){
	switch(colorMode){
		case 1: // LORI D
			var v = Random.Range(0.1,1.0);
			origColor = renderer.material.color = Color(v,v,v);
			break;
		case 2:	// REDINHO
			origColor = renderer.material.color = new Color(Random.Range(0.7,1.0), 0 , Random.Range(0.0,1.0));
			break;
		case 3: // COLOR
			origColor = renderer.material.color = new Color(Random.Range(0.0,1.0), Random.Range(0.0,1.0) , Random.Range(0.0,1.0));
			break;
		default:
			origColor = renderer.material.color = matAr[oscId];
			break;	
	}	
}



function seeMe(){
	return renderer && renderer.isVisible;
}

function FixedUpdate(){
	curVal = OSCReceiver.messages[oscId] * multiplier + .1;
	if(!seeMe()) return;
	scale();	
}

function scale(){
	transform.localScale = Vector3(curVal,curVal,curVal);
}

function OnCollisionEnter( )
{
	renderer.material.color = Color.white;
}

function OnCollisionExit( )
{
	renderer.material.color = origColor;
}
