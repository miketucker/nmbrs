var bloom;
var bloomIntensity:Number = 1.0;
function Start(){
	bloom = GetComponent("BloomAndFlares");
}

function Update () {
	bloom.bloomIntensity = OSCReceiver.messages[0] * bloomIntensity;
}