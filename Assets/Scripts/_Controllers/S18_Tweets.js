var objAr:GameObject[];
var lightAr:GameObject[];
var maxObjectCount : int = 50;
var waitTime : int = 10;

private var artistAr:Array[];

private var wait : int =  0;
private var objs:Array = [];
private var playMode : int = 0;
public var rad:Number = 6;
private var torque:Number = .1;
public var targetArtist:GameObject;
private var tweetCycle:int = 0;
var balloon:GameObject;
public var followCam:GameObject;
private var camFall:ArtistCamFall;
function Start(){
	camFall = followCam.GetComponent("ArtistCamFall");

}

function Update(){
	if(!targetArtist) setMode(playMode);

	if(!OSCReceiver) return;
	var c:Light;
	for(var i:int = 0; i < 4; i++){
		c = lightAr[i].GetComponent("Light");
		c.intensity = OSCReceiver.messages[i] * 2.0;
	}
}


function GetNewTweet(){

	var tweets:GameObject[] = GameObject.FindGameObjectsWithTag("Tweet");
	if(tweetCycle >= tweets.Length) tweetCycle = 0;
	else if(tweetCycle < 0) tweetCycle = tweets.length-1;

	if(targetArtist == null) {
		targetArtist = tweets[0];
		return;
	} 

	targetArtist = tweets[tweetCycle];
}

public function PushTarget()
{
	if(targetArtist)
	{
		var r:Rigidbody = targetArtist.rigidbody;
		if(r)
		{
			r.angularVelocity = Random.insideUnitSphere * 5;
		}
	}
}

public function PushTargetY()
{
	if(targetArtist)
	{
		var r:Rigidbody = targetArtist.rigidbody;
		if(r)
		{
			r.angularVelocity += targetArtist.transform.up * 5.0f;
		}
	}
}



function setMode(n){
	playMode = n;
	GetNewTweet();
	if(!targetArtist) return;
	camFall.target = targetArtist.transform;
	camFall.dist = targetArtist.transform.localScale.y * -8;
}

function clearAll(){
	while(objs.length > 0)
		Destroy(objs.pop());
}


function OnGUI(){
	var e : Event = Event.current;
    if (e.isKey && e.type == EventType.KeyDown) {
        keyEvent(e);
    }
}

function keyEvent(e:Event){
	switch(e.keyCode){
		case KeyCode.F: tweetCycle --; setMode(0); break;
		case KeyCode.G: tweetCycle ++;setMode(1); break;
		case KeyCode.H: setMode(2); break;
		case KeyCode.J: setMode(3); break;	
		case KeyCode.K: SendMessage("PushTargetY"); break;	
		case KeyCode.L: SendMessage("PushTarget"); break;	
		case KeyCode.Semicolon: SendMessage("AddInstructionTweet"); break;	
	}
}
