var skullMats:Material[];
var skull:GameObject;
var redinho:GameObject;
var ballCount :int;
var lightAr:GameObject[];

var dupeAr : GameObject[];
private var objs:Array = [];
private var d:Number = 10;
function Start(){
	

	for (var i = 0; i < ballCount; i++){
		var dist = Random.Range(2.0,5.0);
		var p = Random.Range(-3.16,3.16);
		
		var _x = Mathf.Cos(p) * dist;
		var _z = Mathf.Sin(p) * dist;
		
		var obj:GameObject = Instantiate(dupeAr[Random.Range(0,dupeAr.length)],Vector3(_x,Random.Range(-10,10),_z), Quaternion.identity);
		obj.SetActive(true);
		obj.transform.localScale = Vector3.one * Random.Range(0.001,0.03);
		obj.transform.Rotate(Random.Range(0,360),Random.Range(0,360),Random.Range(0,360));
		objs.push(obj);
	}	
}

function OnGUI(){
	var e : Event = Event.current;
    if (e.isKey && e.type == EventType.KeyDown) {
        keyEvent(e);
    }
}

function keyEvent(e:Event){
	switch(e.keyCode){
		case KeyCode.J: setMode(0); break;
		case KeyCode.K: setMode(1); break;
		case KeyCode.L: setMode(2); break;	
		case KeyCode.L: setMode(2); break;	
		case KeyCode.Semicolon: SwapHead(); break;
	}
}

function SwapHead()
{
	if(skull.activeInHierarchy)
	{
		skull.SetActive(false);
		redinho.SetActive(true);
	} else 
	{
		skull.SetActive(false);
		redinho.SetActive(true);
	}
}

function setMode(n){
	playMode = n;
	skull.renderer.material = skullMats[n];
	redinho.renderer.material = skullMats[n];
}

