var count:int = 30;
var circ:GameObject;
var rows:int = 5;
public static var tickSpeed:Number = .001;
public var tick:Number = .001;
public static var startTime:Number;
public var height:Number = 20;
public var rot:Number = 1;
public static var rotSpeed:Number;
var radRate:Number = 4;

function Start(){
	tickSpeed = tick;
	rotSpeed = rot;
	startTime = Random.Range(0,10);
	for(var i:int = 0; i < rows; i ++){
		var g:GameObject = Instantiate(circ);
		var n:NeonCircle = g.GetComponent('NeonCircle');
		n.count = count + i * 5;
		n.height = height;
		n.rad += i * radRate;
		n.rotSpeed += i * .2;

	}
	setMode(0);
}

function keyEvent(e:Event){
	switch(e.keyCode){
		case KeyCode.J: setMode(0); break;
		case KeyCode.K: setMode(1); break;
		case KeyCode.L: setMode(2); break;	
		case KeyCode.Semicolon: setMode(3); break;	
		case KeyCode.H: setMode(4); break;
	}
}

function setMode(n:int){
	mode = n;

	var ar = GameObject.FindGameObjectsWithTag("Tube");

	for(var g:GameObject in ar){
		g.GetComponent("NeonTubeColor").setMode(n);
	}
}

function OnGUI(){
	var e : Event = Event.current;
    if (e.isKey && e.type == EventType.KeyDown) {
        keyEvent(e);
    }
}