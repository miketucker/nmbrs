var target:GameObject;
var wallMat:Material;
private var bump:StartBump;
private var sp:Vector3;
private var os:float;
private var fp:float;
function Start(){
	sp = target.transform.position;
	bump = target.GetComponent("StartBump");
	os = wallMat.GetFloat("_Shininess");
	fp = wallMat.GetFloat("_FrezPow");
}
function Update () {
	wallMat.SetFloat("_Shininess", .5 - OSCReceiver.messages[0]);
	wallMat.SetFloat("_FrezPow",  OSCReceiver.messages[1] * 2048.0);
}


function OnGUI(){
	var e : Event = Event.current;
    if (e.isKey && e.type == EventType.KeyDown) {
        keyEvent(e);
    }
}

function ResetScene(){
	target.transform.position = sp;
	bump.Bump();
}

function keyEvent(e:Event){
	switch(e.keyCode){
		case KeyCode.Backspace: ResetScene(); break;
	}
}
