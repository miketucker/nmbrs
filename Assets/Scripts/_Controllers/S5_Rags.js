var cloneContainer:GameObject;
var objAr:GameObject[];
var obj_tech:GameObject[];
var glass_fire:GameObject[];
var glass_gold:GameObject[];
var glass_gun:GameObject[];
var glass_rose:GameObject[];
var glass_sunset:GameObject[];
var diamonds:GameObject[];
var rustie_gems:GameObject[];
var ragDolls:GameObject[];
var targetLight:Light;
private var rigidAr:Component[];
private var d:Number = 25.0;
private var h:Number = 400;
private var objs:Array = [];
private var objCount = 700;
private var ragRigid:Rigidbody;
private var lastSmack:Number = 0.0;
private var ragDrag:Number = 0.8;
private var doSmack = false;
private var playMode : int = 0;
private var offCount : Number = 0.0;
var physicMaterial:PhysicMaterial;
public static var ragDoll:GameObject;

function getHeight(){
	return h;
}

function SetDoll(i:int){
	ragDoll = ragDolls[i];
	CamFall.autoUpdate = true;
	ragRigid = ragDolls[i].GetComponent("Rigidbody");
}

function ResetPhysics(){
	for(var i=0; i < ragDolls.length; i++){

		ragRigid = ragDolls[i].GetComponent("Rigidbody");
		ragRigid.drag = ragDrag;
		var cc = ragDolls[i].GetComponentsInChildren(CapsuleCollider);
		for(var c in cc){
			c.material = physicMaterial;
			Debug.Log('add physic material');
		}

		rigidAr = ragDolls[i].GetComponentsInChildren(Rigidbody);
		for (var rag : Rigidbody in rigidAr) {
			rag.drag = ragDrag;
			rag.mass = 1;
			rag.velocity = Vector3.zero;
		}
	}
		
}

function Start(){
	SetDoll(0);
	ResetPhysics();
	resetRagDoll();
	
	for(var j = 0; j < 15; j++){
		var g:Light = Instantiate(targetLight,Vector3(Random.Range(-d,d),Random.Range(10,h),Random.Range(-d,d)), Quaternion.identity);
		// g.SetActive(true);
	}
	
	setMode(playMode);
	
}

function clearAll(){
	while(objs.length > 0)
		Destroy(objs.pop());
		
}

function setMode(n){
	playMode = n;
	var i = 0;
	clearAll();
	
	if(n==0 || n > 6){
		for(i =0; i < objCount; i++){
			spawnTarget();	
		}	
	} else {
		
		var c = objCount;
		if(n==1) c *=.5;
		
		for(i=0; i < c; i++){
			spawnTarget();	
		}	
		
		for(i = 0; i < objs.length; i++){
			if(objs[i].GetComponent('SoundMaterial')) objs[i].GetComponent('SoundMaterial').setMode(n);	
		}
	}
	
}

function OnGUI(){
	var e : Event = Event.current;
    if (e.isKey && e.type == EventType.KeyDown) {
        keyEvent(e);
    }
}

function keyEvent(e:Event){	
	Debug.Log('KEY DOWN');
	switch(e.keyCode){ // QWERT CHANGE DOLLS
		case KeyCode.Q: SetDoll(0); break;
		case KeyCode.W: SetDoll(1); break;
		case KeyCode.E: SetDoll(2); break;
		case KeyCode.R: SetDoll(3); break;
		case KeyCode.T: SetDoll(4); break;

		// = BACKSPACE SMACK RAGS

		case KeyCode.Equals: smackRagDoll(); break;
		case KeyCode.Backspace: resetRagDoll(); break;


		// DIAMONDS
		case KeyCode.G: setMode(12); break;
		case KeyCode.Y: setMode(11); break;
		
		//GLASS UIOPVKL GLASS
		case KeyCode.U: setMode(3); break;
		case KeyCode.I: setMode(4); break;
		case KeyCode.O: setMode(5); break;
		case KeyCode.P: setMode(6); break;
		case KeyCode.V: setMode(7); break;
		case KeyCode.K: setMode(1); break;
		case KeyCode.L: setMode(2); break;	
		case KeyCode.Semicolon: setMode(3); break;	

		// COLORED SHAPES
		case KeyCode.B: setMode(8); break;
		case KeyCode.N: setMode(9); break;
		case KeyCode.M: setMode(10); break;
		case KeyCode.J: setMode(0); break;

	}
}

function Update () {
	
	if(ragDoll && ragDoll.transform.position.y < -10) resetRagDoll();
	if(!ragDoll){
		offCount += .1;
		for(var i = 0; i < objs.length;i++){
			objs[i].transform.position.y = objs[i].transform.position.y - .1;	
		}
		
		if(offCount > h + 40) resetRagDoll();
	}
}


function smackRagDoll(){
	ragRigid.AddExplosionForce(8000.0, ragDoll.transform.position, 0, 3.0);
	lastSmack = Time.time;
}

function resetRagDoll(){
	var d:Number = 10.0;
	for(var i:int=0; i < ragDolls.length;i++){
		r = ragDolls[i];
		r.transform.position.y = h - Random.Range(0,20);
		r.transform.Rotate(Vector3.up * Random.Range(0,360.0));
		r.transform.Rotate(Vector3.right * Random.Range(0,45.0));
		r.transform.Rotate(Vector3.forward * Random.Range(0,45.0));
		r.transform.position.x = Random.Range(-d,d);
		r.transform.position.z = Random.Range(-d,d);
	}
	
}

function spawnTarget(){
	var ar;
	switch(playMode){
		case 12: ar = rustie_gems; break;
		case 11: ar = diamonds; break;
		case 7: ar = glass_sunset; break;
		case 6: ar = glass_sunset; break;
		case 5: ar = glass_gold; break;
		case 4: ar = glass_rose; break;
		case 3: ar = glass_fire; break;
		case 2: ar = glass_gun; break;
		case 1: ar = obj_tech; break;
		default: ar = objAr;
	}
	
	var targ:GameObject = ar[Random.Range(0,ar.length)];
	
	var q = Quaternion.identity;
	q.x = Random.value;
	q.y = Random.value;
	q.z = Random.value;
	var obj:GameObject = Instantiate(targ,Vector3(Random.Range(-d,d),Random.Range(10,h),Random.Range(-d,d)), q);
	obj.SetActive(true);
	obj.transform.parent = cloneContainer.transform;
	if(( playMode > 0 && playMode < 7 ) || playMode > 10){
		if(ragDoll){
			// var rb:Rigidbody = obj.AddComponent("Rigidbody");
			// rb.useGravity = false;
			// obj.AddComponent("MeshCollider");
			// obj.AddComponent("ObjMusicSpin");
//			r = obj.GetComponent("Rigidbody");
//			r.isKinematic = true;
		}
		 obj.transform.localScale *= Random.Range(0.1,3.0);
		 obj.transform.Rotate(Random.Range(0.0,360.0),Random.Range(0.0,360.0),Random.Range(0.0,360.0));
	} else {
		// Debug.Log('colored');
		// obj.rigidbody.isKinematic = false;
		// obj.rigidbody.useGravity = false;
		// obj.AddComponent("ObjMusicSpin");
		
		var p = playMode;
		if(p > 6) p -= 6;
		obj.transform.localScale = Vector3.one * Random.Range(3.0,10.0);
		var sm:SoundMaterial = obj.GetComponent("SoundMaterial");
		if(sm){
			sm.scaleMin = Random.Range(.5,2.0);
			sm.scaleMax = sm.scaleMin + 4.0;
			sm.setMode(p);

		} 
	}
	objs.push(obj);
}
