var starfield:Starfield;
function Start(){
	if(starfield)	starfield.setMode(0);

}

function keyEvent(e:Event){
	switch(e.keyCode){
		case KeyCode.U: Restart(); break;
		case KeyCode.J: starfield.setMode(0); break;
		case KeyCode.K: starfield.setMode(1); break;
		case KeyCode.L: starfield.setMode(2); break;	
		case KeyCode.Semicolon: starfield.setMode(3); break;	
		case KeyCode.H: starfield.setMode(4); break;
	}
}

function OnGUI(){
	var e : Event = Event.current;
    if (e.isKey && e.type == EventType.KeyDown) {
        keyEvent(e);
    }
}

function Restart(){

	var ar = GameObject.FindGameObjectsWithTag("Star");

	for(var g:GameObject in ar){
		g.GetComponent("OrbTransitionIn").Restart();
	}
}