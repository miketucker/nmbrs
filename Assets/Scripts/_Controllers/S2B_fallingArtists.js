var objAr:GameObject[];
var lightAr:GameObject[];
var maxObjectCount : int = 50;
var waitTime : int = 10;

private var artistAr:Array[];

private var wait : int =  0;
private var objs:Array = [];
private var playMode : int = 0;
public var rad:Number = 6;
private var torque:Number = .1;
public var targetArtist:GameObject;

var balloon:GameObject;
public var followCam:GameObject;
private var camFall:ArtistCamFall;
function Start(){
	camFall = followCam.GetComponent("ArtistCamFall");
	artistAr = new Array[objAr.length];
	for(var i:int = 0; i < objAr.length; i++){
		artistAr[i] = new Array();
	}

	SpawnLoop();
	

}

function SpawnLoop(){
	while(true){
		if(objs.length > maxObjectCount){	
			Destroy(objs.shift());
		}
		spawnObject();
		yield WaitForSeconds(Random.Range(2,3));
	}
}

function Update(){
	if(!targetArtist) setMode(playMode);

	if(!OSCReceiver) return;
	var c:Light;
	for(var i:int = 0; i < 4; i++){
		c = lightAr[i].GetComponent("Light");
		c.intensity = OSCReceiver.messages[i] * 2.0;
	}
}


function spawnObject(){

	
	var obj;
	var n:int = Random.Range(0,objAr.length);
	obj = objAr[n];

	var o:GameObject = Instantiate(obj,Vector3(Random.Range(-rad,rad),15,Random.Range(-rad,rad)),Quaternion.identity);
	o.SetActive(true);
	var ar:Array = artistAr[n];
	ar.push(o);

	if(!targetArtist){ 
		setMode(n);
	}

	o.transform.Rotate(Random.Range(-90,90),Random.Range(-180,180),Random.Range(-90,90));
	var s;
	s = Random.Range(1,4);
	o.transform.localScale = Vector3(s,s,s);
	o.AddComponent("BalloonAutoKill");
	// o.AddComponent("MeshCollider");	
	// o.GetComponent("MeshCollider").convex = true;
	var r:Rigidbody = o.AddComponent("Rigidbody");	
	r.angularVelocity = Vector3(Random.Range(-1.0,1.0),Random.Range(-1.0,1.0),Random.Range(-1.0,1.0));
	r.collisionDetectionMode = CollisionDetectionMode.Continuous;
	// o.renderer.material.color = HSBColor.ToColor(new HSBColor(Random.value*.1,1,1,1));
	// o.rigidbody.mass = .001;
	o.rigidbody.drag = 4;
	// o.rigidbody.AddRelativeTorque(Random.Range(-torque,torque),Random.Range(-torque,torque),Random.Range(-torque,torque));
	o.rigidbody.angularDrag = 0;

	objs.push(o);
}


function setMode(n){
	playMode = n;
	GetNewArtist();
	if(!targetArtist) return;
	camFall.target = targetArtist.transform;
	camFall.dist = targetArtist.transform.localScale.y * 12;
	Debug.Log('dist '+targetArtist.transform.localScale.y * 8);
}

function GetNewArtist(){
	if(artistAr[playMode].length < 1) return;
	targetArtist = artistAr[playMode][artistAr[playMode].length-1];

}

function clearAll(){
	while(objs.length > 0)
		Destroy(objs.pop());
}


function OnGUI(){
	var e : Event = Event.current;
    if (e.isKey && e.type == EventType.KeyDown) {
        keyEvent(e);
    }
}

function keyEvent(e:Event){
	switch(e.keyCode){
		case KeyCode.F: setMode(0); break;
		case KeyCode.G: setMode(1); break;
		case KeyCode.H: setMode(2); break;
		case KeyCode.J: setMode(3); break;	
		case KeyCode.K: setMode(4); break;	
		case KeyCode.L: setMode(5); break;	
		case KeyCode.Semicolon: setMode(6); break;	
	}
}
