
var targ:GameObject;
private var objs : Array = [];
private var tick : int = 0;
private var maxCount = 500;
private var d:Number = 0;
private var delay:Number = 1;
private var vy:Number = .5;
private var objAr:Array = [];
private var spawnPos:Vector3 = new Vector3(0,6,0);
private var nAr:Array = [];
private var collide:MeshCollider;
private var playMode : int = 0;
function Start(){
	for(var i=1; i < 10; i++)
		nAr.push(GameObject.Find("n"+i));
	
	setNum(9);
	
}

function setNum(n){
	n-=1;
	for(var i = 0; i < nAr.length;i++){
		if(i!=n){
			nAr[i].GetComponent("MeshCollider").sharedMesh = new Mesh();
		} else {
			nAr[i].GetComponent("MeshCollider").sharedMesh = nAr[i].GetComponent(MeshFilter).mesh;
		}
	}
	
	var h = 5.0;
	
	for(i = 0; i < objs.length; i++){
		objs[i].GetComponent("Rigidbody").velocity = (Vector3.zero - objs[i].transform.position) * -10.0;
	}
	
	nextNum(n);
}

function nextNum(n){
	yield WaitForSeconds(10);
	if(n==0) n = 9;
	setNum(n);	
}

function FixedUpdate () {
	
	if(objs.length>maxCount){
		var o = objs.shift();
		MoveToTop(o);
		objs.push(o);
	} else {
		spawnArea();
	}
	
	
	
}

function MoveToTop(target:GameObject){
	var v:Vector3 = new Vector3();
	v.x = spawnPos.x + Random.Range(-1.0,1.0);
	v.y = spawnPos.y;
	v.z = spawnPos.z + Random.Range(-1.0,1.0);
	target.transform.position = v;
}

function spawnArea(){
	var v:Vector3 = new Vector3();
	v.x = spawnPos.x + Random.Range(-1.0,1.0);
	v.y = spawnPos.y;
	v.z = spawnPos.z + Random.Range(-1.0,1.0);
	
	var obj:GameObject = Instantiate(targ,v, Quaternion.identity);
	obj.SetActive(true);
	obj.GetComponent('FillNumberBallBehaviour').setMode(playMode);	
	objs.push(obj);
}

function OnGUI(){
	var e : Event = Event.current;
    if (e.isKey && e.type == EventType.KeyDown) {
        keyEvent(e);
    }
}

function keyEvent(e:Event){
	switch(e.keyCode){
		case KeyCode.J: setMode(0); break;
		case KeyCode.K: setMode(1); break;
		case KeyCode.L: setMode(2); break;	
		case KeyCode.Semicolon: setMode(3); break;	
	}
}

function setMode(n){
	playMode = n;
	for(var i = 0; i < objs.length; i++){
		 objs[i].GetComponent('FillNumberBallBehaviour').setMode(n);	
	}	
}
