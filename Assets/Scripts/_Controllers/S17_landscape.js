var cubemaps:Cubemap[];
var targetMat:Material;

private var keys:Array = ['t','y','u','i','o','p','g','h','j','k','l','b','n','m'];

function OnGUI(){
	var i:int = 0;
	for(var k:String in keys){
		if(Input.GetKeyDown(keys[i]) && cubemaps.length > i) targetMat.SetTexture("_Cube",cubemaps[i]);
		i++;
	}
}

function Update () {
}