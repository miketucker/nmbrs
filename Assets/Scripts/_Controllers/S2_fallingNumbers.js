var objAr:GameObject[];
var lightAr:GameObject[];
var matAr:Material[];
var maxObjectCount : int = 50;
var waitTime : int = 10;
private var wait : int =  0;
private var objs:Array = [];
private var playMode : int = 0;
public var rad:Number = 4;
private var torque:Number = .1;
public var mode:int = 0;

var balloon:GameObject;

function Start(){
	SpawnLoop();
}

function SpawnLoop(){
	while(true){
		if(objs.length > maxObjectCount){	
			Destroy(objs.shift());
		}
		spawnObject();
		if(Random.value < .5) SpawnBalloon();
		yield WaitForSeconds(Random.Range(1,1.5));
	}
}

function Update(){
	if(!OSCReceiver) return;
	var c:Light;
	for(var i:int = 0; i < 4; i++){
		c = lightAr[i].GetComponent("Light");
		c.intensity = OSCReceiver.messages[i] * 2.0;
	}
}

function setMode(n){
	var balloons = GameObject.FindGameObjectsWithTag("Balloon");
	mode = n;
	for(var g:GameObject in balloons){
		g.renderer.material.color = ColorSchemes.GetByMode(mode);
	}	
}

function keyEvent(e:Event){
	switch(e.keyCode){
		case KeyCode.J: setMode(0); break;
		case KeyCode.K: setMode(1); break;
		case KeyCode.L: setMode(2); break;	
		case KeyCode.Semicolon: setMode(3); break;	
		case KeyCode.H: setMode(4); break;	
		case KeyCode.G: setMode(5); break;	
	}
}

function SpawnBalloon(){
	var o:GameObject = Instantiate(balloon,Vector3(Random.Range(-rad,rad),-10,Random.Range(-rad,rad)),Quaternion.identity);
	o.SetActive(true);
	o.renderer.material.color = ColorSchemes.GetByMode(mode);
	// o.renderer.material = matAr[Random.Range(0,matAr.length)];

	// o.renderer.material.color = HSBColor.ToColor(new HSBColor(Random.value*.1,1,1,1));
}


function spawnObject(){

	
	var obj;
	
	obj = objAr[Random.Range(0,objAr.length)];
	var o:GameObject = Instantiate(obj,Vector3(Random.Range(-rad,rad),10,Random.Range(-rad,rad)),Quaternion.identity);
	o.SetActive(true);
	o.transform.Rotate(Random.Range(-90,90),Random.Range(-45,45),Random.Range(-90,90));
		var s;
		s = Random.Range(0.3,4);
		o.transform.localScale = Vector3(s,s,s);
		o.AddComponent("BalloonAutoKill");
		o.AddComponent("StartSpin");
		o.renderer.material = matAr[Random.Range(0,matAr.length)];

		// o.AddComponent("MeshCollider");	
		// o.GetComponent("MeshCollider").convex = true;
		o.AddComponent("Rigidbody");	
		o.GetComponent("Rigidbody").collisionDetectionMode = CollisionDetectionMode.Continuous;
		o.renderer.material.color = HSBColor.ToColor(new HSBColor(Random.value*.1,1,1,1));
		// o.rigidbody.mass = .001;
		o.rigidbody.drag = 4;
		// o.rigidbody.AddRelativeTorque(Random.Range(-torque,torque),Random.Range(-torque,torque),Random.Range(-torque,torque));
		o.rigidbody.angularDrag = .1;

	objs.push(o);
}


// function setMode(n){
// 	playMode = n;
// 	if(n < 4){
// 		for(var i = 0; i < objs.length; i++){
// 			WaitForSeconds(1);
// 			objs[i].GetComponent('SoundMaterial').setMode(n);	
// 		}	
// 	} else {
// 		clearAll();	
// 	}
// }

function clearAll(){
	while(objs.length > 0)
		Destroy(objs.pop());
}


function OnGUI(){
	var e : Event = Event.current;
    if (e.isKey && e.type == EventType.KeyDown) {
        keyEvent(e);
    }
}

// function keyEvent(e:Event){
// 	switch(e.keyCode){
// 		case KeyCode.L: setMode(6); break;
// 		case KeyCode.K: setMode(5); break;
// 		case KeyCode.J: setMode(4); break;
// 		case KeyCode.V: setMode(0); break;
// 		case KeyCode.B: setMode(1); break;
// 		case KeyCode.N: setMode(2); break;	
// 		case KeyCode.M: setMode(3); break;	
// 	}
// }