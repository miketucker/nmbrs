var objAr:GameObject[];
var obj_tech:GameObject[];
var glass_fire:GameObject[];
var glass_gold:GameObject[];
var glass_gun:GameObject[];
var glass_rose:GameObject[];
var glass_sunset:GameObject[];
var diamonds:GameObject[];
var ragDoll:GameObject;
var targetLight:Light;
private var rigidAr:Component[];
private var d:Number = 15.0;
private var h:Number = 300;
private var objs:Array = [];
private var objCount = 700;
private var ragRigid:Rigidbody;
private var lastSmack:Number = 0.0;
private var ragDrag:Number = 0.5;
private var doSmack = false;
private var playMode : int = 1;
private var offCount : Number = 0.0;
function getHeight(){
	return h;
}

function Start(){

	
	for(var j = 0; j < 15; j++){
		var g:Light = Instantiate(targetLight,Vector3(Random.Range(-d,d),Random.Range(10,h),Random.Range(-d,d)), Quaternion.identity);
		// g.SetActive(true);
	}
	
	setMode(playMode);
	
}

function clearAll(){
	while(objs.length > 0)
		Destroy(objs.pop());
		
}

function setMode(n){
	playMode = n;
	var i = 0;
	clearAll();
	
	if(n==0 || n > 6){
		for(i =0; i < objCount; i++){
			spawnTarget();	
		}	
	} else {
		
		var c = objCount;
		if(n==1) c *=.5;
		
		for(i=0; i < c; i++){
			spawnTarget();	
		}	
		
		for(i = 0; i < objs.length; i++){
			if(objs[i].GetComponent('SoundMaterial')) objs[i].GetComponent('SoundMaterial').setMode(n);	
		}
	}
	
}

function OnGUI(){
	var e : Event = Event.current;
    if (e.isKey && e.type == EventType.KeyDown) {
        keyEvent(e);
    }
}

function keyEvent(e:Event){	
	Debug.Log('KEY DOWN');
	switch(e.keyCode){
		case KeyCode.Equals: smackRagDoll(); break;
		case KeyCode.Backspace: resetRagDoll(); break;
		case KeyCode.Y: setMode(11); break;
		
		case KeyCode.U: setMode(3); break;
		case KeyCode.I: setMode(4); break;
		case KeyCode.O: setMode(5); break;
		case KeyCode.P: setMode(6); break;

		case KeyCode.V: setMode(7); break;
		case KeyCode.B: setMode(8); break;
		case KeyCode.N: setMode(9); break;
		case KeyCode.M: setMode(10); break;
				
		case KeyCode.J: setMode(0); break;
		case KeyCode.K: setMode(1); break;
		case KeyCode.L: setMode(2); break;	
		case KeyCode.Semicolon: setMode(3); break;	
	}
}

function Update () {
	
	if(ragDoll && ragDoll.transform.position.y < -10) resetRagDoll();
	if(!ragDoll){
		offCount += .1;
		for(var i = 0; i < objs.length;i++){
			objs[i].transform.position.y = objs[i].transform.position.y - .1;	
		}
		
		if(offCount > h + 40) resetRagDoll();
	}
}


function smackRagDoll(){
	ragRigid.AddExplosionForce(8000.0, ragDoll.transform.position, 0, 3.0);
	lastSmack = Time.time;
}

function resetRagDoll(){
	if(ragDoll){
		ragDoll.transform.position.y = h + 20;
		ragDoll.transform.position.x = 0;
		ragDoll.transform.position.z = 0;	
	} else {
	 	offCount = 0;
	 	for(var i = 0; i < objs.length;i++)
			objs[i].transform.position.y = Random.Range(10,h);	

	}
	
}

function spawnTarget(){
	var ar;
	switch(playMode){
		case 11: ar = diamonds; break;
		case 7: ar = glass_sunset; break;
		case 6: ar = glass_sunset; break;
		case 5: ar = glass_gold; break;
		case 4: ar = glass_rose; break;
		case 3: ar = glass_fire; break;
		case 2: ar = glass_gun; break;
		case 1: ar = obj_tech; break;
		default: ar = objAr;
	}
	
	var targ:GameObject = ar[Random.Range(0,ar.length)];
	
	var q = Quaternion.identity;
	q.x = Random.value;
	q.y = Random.value;
	q.z = Random.value;
	var obj:GameObject = Instantiate(targ,Vector3(Random.Range(-d,d),Random.Range(-10,h),Random.Range(-d,d)), q);
	obj.SetActive(true);
	if(( playMode > 0 && playMode < 7 ) || playMode == 11){
		obj.AddComponent("ObjMusicSpin");

		 obj.transform.localScale *= Random.Range(1.0,2.0);
		 obj.transform.Rotate(Random.Range(0.0,360.0),Random.Range(0.0,360.0),Random.Range(0.0,360.0));
	} else {
		var p = playMode;
		if(p > 6) p -= 6;
		if(obj.GetComponent("SoundMaterial")) obj.GetComponent("SoundMaterial").setMode(p);
	}
	objs.push(obj);
}
