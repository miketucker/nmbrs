var speed:Number = 0;
var rSpeed:Number = 0;
function Start(){
	speed = Random.Range(0.01, 0.1);
	rSpeed = Random.Range(-50,50);
}

function Update () {
	transform.position.y -= speed;
	transform.Rotate(Vector3.forward,Time.deltaTime * rSpeed,Space.World);
	if(transform.position.y < -12) Destroy(gameObject);
}