var lightAr;
function Start(){
	lightAr = GameObject.FindGameObjectsWithTag("Light");

}

function Update () {
	if(!OSCReceiver || !OSCReceiver.messages[0]) return;
	// for(var g:GameObject in lightAr){
	// 	g.light.range = OSCReceiver.messages[0] * 50;
	// }
	if(OSCReceiver.messages[0] > 1 - KeyboardManager.strobeLevel){
		for(var g:GameObject in lightAr){
			g.GetComponent("LightShift").h += OSCReceiver.messages[0];
			if(g.GetComponent("LightShift").h > 1 ) g.GetComponent("LightShift").h -= 1;
		}
	}
}