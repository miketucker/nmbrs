var spinStrobe:boolean = false;
var tick:Number = 0;
var rate:Number = 0.01;
var amp:Number = 0.01;
var strobeAmp:Number = 10;
var curVec:Vector3;
var vec:Vector3;
// var srate:Number = 0.01;

function Update () {
	var mult:Number = amp;
	if(spinStrobe && OSCReceiver.messages[0] > 1 - KeyboardManager.strobeLevel) mult += OSCReceiver.messages[0] * strobeAmp;
	vec = Vector3(Mathf.Sin(tick) * mult,Mathf.Cos(tick*2)  * mult,Mathf.Cos(-tick)  * mult);
	curVec += (vec - curVec) * .5;

	transform.Rotate(curVec);
	tick += rate;
	// rate = srate;
}