private var mat:Material;
private var hsb:HSBColor;
private var h:Number = 0;
public var speed:Number = 0.001;
public var offset:Number = .5;
function Start(){
	mat = renderer.material;	
	hsb = new HSBColor(Color.white);
	hsb.s = 1;
	hsb.h = offset;
}

function Update () {
	h += speed;
	if(h>= 1) h = 0;
	hsb.h = h;
	mat.SetColor("_SpecColor",HSBColor.ToColor(hsb));
	mat.SetColor("_Emission",HSBColor.ToColor(hsb));

}