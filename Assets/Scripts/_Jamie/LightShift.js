public var hsb:HSBColor;
public var h:Number = 0;
public var curh:Number = 0;
public var speed:Number = 0.001;
public var offset:Number = .5;

public var colorStart:Number = .2;
public var colorEnd:Number = .4;

function Start(){
	hsb = new HSBColor(.5,1,1);
	// hsb.s = 1;
	// hsb.b = 1;
	// hsb.a = 1;
	h = offset;
	// Debug.Log(HSBColor.ToColor(new HSBColor(Color.white)));
}

function Update () {
	h += speed;
	curh += (h - curh) * .1;
	if(h>= colorEnd) h = colorStart;
	if(curh>= colorEnd) curh = colorStart;

	hsb.h = curh;
	light.color =HSBColor.ToColor(hsb);
}