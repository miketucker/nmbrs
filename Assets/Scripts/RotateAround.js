private var startY:Number;
var upAndDown : Number = 3.0;
var randomSpeed:boolean = false;
public var spinSpeed:float = 5.0f;
public var minSpinSpeed:float = 1.0f;
function Start(){
	spinSpeed = Random.Range(minSpinSpeed, spinSpeed);
	startY = transform.position.y;
}

function Update () {
	    transform.RotateAround (Vector3.zero, Vector3.up, spinSpeed * Time.deltaTime * KeyboardManager.spinMultiplier);
	    transform.LookAt(Vector3.zero);
	    if(upAndDown > 0) transform.position.y = startY + Mathf.Sin(Time.time/(40-spinSpeed)) * upAndDown;
}