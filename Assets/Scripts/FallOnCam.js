var targets : GameObject[];

private var objs : Array = [];
private var tick : int = 0;
private var maxCount = 100;
private var d:Number = 5;
private var delay:Number = 20;
function Update () {
	
	if(objs.length>maxCount){
		var o = objs.shift();
		Destroy(o);
	}
	
	if( tick == delay){
		spawnTarget();
		tick = 0;
	} 
	tick++;
	
}

function spawnTarget(){
	var q = new Quaternion();
	q.x = Random.value;
	q.y = Random.value;
	q.z = Random.value;
	
	var obj = Instantiate(targets[Random.Range(0,targets.length)],Vector3(Random.Range(-d,d),Random.Range(50,60),Random.Range(-d,d)), q);
	obj.AddComponent(Rigidbody);
	obj.GetComponent(Rigidbody).collisionDetectionMode = CollisionDetectionMode.Continuous;
	var s = Random.Range(0.5,3.0);
	obj.transform.localScale = Vector3(s,s,s);
	objs.push(obj);
}
