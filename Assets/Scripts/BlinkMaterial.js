private var origColor:Color;
private var blink:Color = Color(1.0,1.0,1.0,1.0);
private var collide = false;
private var FieldRadius : float = 15.0;
private var FieldForce : float = 5.0;
private var origPos : Vector3 = new Vector3();
function Start(){
	origColor = renderer.material.color;
	collide = Random.Range(0,40)==2;	
//	if(collide) origColor = blink;
	if(collide){
		origPos = Vector3(1,1,1);
	} else {
		
	origPos.x = transform.position.x;
	origPos.y = transform.position.y;
	origPos.z = transform.position.z;
	}
}

function FixedUpdate(){
	if(collide) collideObj();
	transform.position.x += (origPos.x - transform.position.x) * .01; 
	transform.position.y += (origPos.y - transform.position.y) * .01; 
	transform.position.z += (origPos.z - transform.position.z) * .01; 
}

function collideObj(){
	var colliders : Collider[];
    var rigidbody : Rigidbody;

    colliders=Physics.OverlapSphere (transform.position, FieldRadius);
    
	for(var c in colliders){
    	if(c != collider)
    	c.rigidbody.AddExplosionForce (FieldForce * -1, transform.position, FieldRadius);
    }
	
}

function OnCollisionEnter( )
{
	renderer.material.color = blink;
}

function OnCollisionExit( )
{
	renderer.material.color = origColor;
}
