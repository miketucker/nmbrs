private var curVal : Number = 0.0;
public var multiplier : Number = 4.0;
private var minNum : Number = 0.1;
public var channel:int = 0;
public var minAmp:float = 0;
private var minVal:float;
public var maxAmp:float = 1;
private var lamp;
private var lampIntensity:float;
function Start(){
	lamp = GetComponent("Light");
	lampIntensity = lamp.intensity * maxAmp;
	minVal = lampIntensity * minAmp;
}


function Update () {
	
	curVal = OSCReceiver.messages[channel] * multiplier + minNum;
	lamp.intensity = minVal + ((lampIntensity - minVal) * curVal);
}