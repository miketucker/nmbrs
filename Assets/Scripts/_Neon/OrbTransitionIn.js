var inSlow:Number = .1;
var op:Vector3;
var sp:Vector3;
var duration:float;
private var velocity:Vector3 = Vector3.zero;
private var startTime:Number;
private var out:boolean = false;
private var delay:boolean = false;
function Start(){
	op = transform.position;
	sp = op;
	sp.Normalize();
	duration = 1.0;

	sp *= Random.Range(100,500);
	inSlow = Random.Range(0.01,.2);
	transform.position = sp;

	delay=true;
	yield WaitForSeconds(.1 * (op-Vector3.zero).magnitude);
	startTime = Time.time;
	delay=false;
	// rigidbody.isKinematic = true;
	rigidbody.drag = 0.1;
	GoToCenter();
}

function GoToCenter(){
	while(true){
		rigidbody.velocity = -transform.position*.2;

		yield WaitForSeconds(Random.Range(1,10));
	}
}

function Restart(){
	out = !out;
	velocity = Vector3.zero;

	delay=true;
	yield WaitForSeconds(.1 * (op-Vector3.zero).magnitude);
	startTime = Time.time;
	delay=false;
}

// function Update () {
// 	if(Time.time - startTime > 20 || delay == true){
// 		rigidbody.isKinematic = false;
// 		return;}

// 	if(out) transform.position = Vector3.SmoothDamp(transform.position, sp, velocity, duration);
// 	else transform.position = Vector3.SmoothDamp(transform.position, op, velocity, duration);
// }
