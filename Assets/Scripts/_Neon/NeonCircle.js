var dupe:GameObject;
var count:int = 20;
var rad:Number = 5;
var height:Number = 20;
var rotSpeed:Number = .5;
function Start () {
	rotSpeed = S10_Neon.rotSpeed;
	for(var i=0; i < count; i++){
		var p = Random.Range(-3.16,3.16);
		var _x = Mathf.Cos(p) * rad;
		var _z = Mathf.Sin(p) * rad;
		var g:GameObject = Instantiate(dupe,Vector3(_x, Random.Range(-height,height), _z),Quaternion.identity);
		var s:Number =  Random.Range(.01,.2);
		g.transform.localScale.x = g.transform.localScale.z = s;
		g.transform.localScale.y = s * 15;
		g.transform.parent = transform;
	}
}
