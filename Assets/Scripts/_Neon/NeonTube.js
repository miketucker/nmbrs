var rate:Number = 0.01;
var tick:Number = 0;
var op:Vector3;
private var cp:Vector3;
private var dp:Vector3;
var spin:Vector3 = Vector3(Random.value-.5,Random.value-.5,Random.value-.5);

function Start(){
	rate = S10_Neon.tickSpeed;
	tick = S10_Neon.startTime;

	transform.Rotate(Random.Range(-360,360),Random.Range(-360,360),Random.Range(-360,360));
	op = transform.position;
	cp = op;
	cp *= .9;
	cp.y = 0 ;

}

function Update(){
	transform.position = op - (cp * Mathf.Cos(tick));
	tick += rate * KeyboardManager.spinMultiplier * .3;
	transform.Rotate(spin);
	// transform.RotateAround(transform.InverseTransformPoint(Vector3.zero), Vector3.up, NeonController.rotSpeed);

}