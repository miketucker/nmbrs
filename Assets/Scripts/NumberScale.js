var minNum:Number = 0.4;
var multiplier:int = 2;
var rigid:Rigidbody;
private var matAr:Array = [Color(0.5,1.0,0.5,1.0),Color(1.0,0.0,.5,1.0),Color(0.0,1.0,0.5,1.0),Color(1.0,0.0,1.0,1.0)];
private var oscId:int = 0;
private var origColor : Color = new Color();
private var colorMode : int = 0;
private var origScale : Vector3;

function Start(){
	oscId = Mathf.Floor(Random.Range(0,4));
	origScale = transform.localScale;
	rigid = GetComponent("Rigidbody");
//	updateColor();
}

function setMode(n){
	colorMode = n;
//	updateColor();
}


function updateColor(){
	switch(colorMode){
		case 1: // LORI D
			var v = Random.Range(0.1,1.0);
			origColor = renderer.material.color = Color(v,v,v);
			break;
		case 2:	// REDINHO
			origColor = renderer.material.color = new Color(Random.Range(0.7,1.0), 0 , Random.Range(0.0,1.0));
			break;
		case 3: // COLOR
			origColor = renderer.material.color = new Color(Random.Range(0.0,1.0), Random.Range(0.0,1.0) , Random.Range(0.0,1.0));
			break;
		default:
			origColor = renderer.material.color = matAr[oscId];
			break;	
	}	
}



function Update () {
	var curVal:Number = OSCReceiver.messages[oscId];
	scale(curVal*2);
	
//	if(Random.Range(0,300)==4) {
//		rigid.AddExplosionForce(curVal * 100,transform.position,0,10.0);
////		Debug.Log(curVal * 1000);
//	}
}


function scale(curVal){
	transform.localScale = Vector3(curVal,curVal,curVal) + origScale;
}