var objAr:GameObject[];
var maxObjectCount : int = 50;
var waitTime : int = 10;
private var wait : int =  0;
private var objs:Array = [];
var dist:Number = 3.0;
function Update () {
	if(objs.length > maxObjectCount){	
		Destroy(objs.shift());
	}

	if(wait == 0){
		spawnObject();
		wait = waitTime;
	} else {
		wait --;	
	}
}

function spawnObject(){
	var q = new Quaternion();
	q.x = Random.value;
	q.y = Random.value;
	q.z = Random.value;
	
	var _x = Mathf.Cos(Random.Range(-3.16,3.16)) * dist;
	var _z = Mathf.Sin(Random.Range(-3.16,3.16)) * dist;
	
	var o = Instantiate(objAr[Random.Range(0,objAr.length)],Vector3(_x,Random.Range(10,12),_z),q);
	o.GetComponent("Rigidbody").velocity = Vector3(Random.Range(-1.0,1.0),Random.Range(-1.0,1.0),Random.Range(-1.0,1.0));
	objs.push(o);
}