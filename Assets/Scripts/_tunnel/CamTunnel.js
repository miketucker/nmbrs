/*
This camera smoothes out rotation around the y-axis and height.
Horizontal Distance to the target is always fixed.

There are many different ways to smooth the rotation but doing it this way gives you a lot of control over how the camera behaves.

For every of those smoothed values we calculate the wanted value and the current value.
Then we smooth it using the Lerp function.
Then we apply the smoothed values to the transform's position.
*/
var followRot : boolean;
// The target we are following
// The distance in the x-z plane to the target
var distance = 100.0;
// the height we want the camera to be above the target
var height = 55.0;
// How much we 
var heightDamping = 2.0;
var rotationDamping = 3.0;
public var camDistance:Number = 15.0;
private var rotationSpeed:Number = .7;
private var smooth:Number = .99;
public var target:Transform;
var lockCenter:boolean = false;
var orientation:String = "across";

// Place the script in the Camera-Control group in the component menu
public static var autoUpdate:boolean = false;
public var t:Vector3 = Vector3.zero;
private var sint:Vector3 = Vector3.zero;
public var spinRate:Number = .1;
public var spinDist:Number = 2;
public var easeSpin:Number = 0.01;
private var spin:Number = 0;
@script AddComponentMenu("Camera-Control/Smooth Follow")

function Start(){
    t = target.position;
}


function LateUpdate () {
    spin += spinRate;
    sint.x = Mathf.Sin(spin) * spinDist;
    sint.z = Mathf.Cos(spin) * spinDist;
    t+= ((target.position + sint) - t) * easeSpin;

    if(autoUpdate) target = S5_Rags.ragDoll.transform;
    // Debug.Log('auto update'+target);
    // Early out if we don't have a target
    wantedRotationAngle = target.eulerAngles.y;
    wantedHeight = target.position.y + height;
    currentRotationAngle = transform.eulerAngles.y;
    currentHeight = transform.position.y;
    currentRotationAngle = Mathf.LerpAngle (currentRotationAngle, wantedRotationAngle, rotationDamping * Time.deltaTime);
    currentHeight = Mathf.Lerp (currentHeight, wantedHeight, heightDamping * Time.deltaTime);
    currentRotation = Quaternion.Euler (0, currentRotationAngle, 0);

    transform.position = target.position;
    // transform.position.y  = target.position.y;
    if(lockCenter){
        transform.position.x = 0;
        transform.position.z = 0;
    }
    switch(orientation){
    	case "across":
		    
		   if(followRot)  transform.position -= currentRotation * Vector3.forward * distance * smooth;
		   else {
				transform.position.z -= Mathf.Sin(Time.time*rotationSpeed) * camDistance;
				transform.position.x -= Mathf.Cos(Time.time*rotationSpeed) * camDistance;
		   }
		   break;
		case "down":
		 	transform.position.y += 10;
			break;
		case "up":
		 	transform.position.y -= 10;
			break;
    }
    if(lockCenter){
        transform.LookAt(t);
        // transform.rotation = Quaternion.identity;
        // switch(orientation){
        //     case "down":
        //         transform.Rotate(Vector3(90,0,0));
        //         break;
        //     case "up":
        //         transform.Rotate(Vector3(-90,0,0));
        //         break;
        // }    
    } else {
      transform.LookAt(target.transform);
    }
}