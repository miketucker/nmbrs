/*
This camera smoothes out rotation around the y-axis and height.
Horizontal Distance to the target is always fixed.

There are many different ways to smooth the rotation but doing it this way gives you a lot of control over how the camera behaves.

For every of those smoothed values we calculate the wanted value and the current value.
Then we smooth it using the Lerp function.
Then we apply the smoothed values to the transform's position.
*/
var followRot : boolean;
// The target we are following
var target : Transform;
// The distance in the x-z plane to the target
var distance = 100.0;
// the height we want the camera to be above the target
var height = 55.0;
// How much we 
var heightDamping = 2.0;
var rotationDamping = 3.0;
private var camDistance:Number = 15.0;
private var rotationSpeed:Number = .7;
private var smooth:Number = .99;

var orientation:String = "across";

// Place the script in the Camera-Control group in the component menu
@script AddComponentMenu("Camera-Control/Smooth Follow")

function LateUpdate () {
    // Early out if we don't have a target
    if(!target){
    	var o = GetComponent("MagnetController").getTarget();
    	target = o.transform;
    }
    
    if (!target)
        return;

    wantedRotationAngle = target.eulerAngles.y;
    wantedHeight = target.position.y + height;
    currentRotationAngle = transform.eulerAngles.y;
    currentHeight = transform.position.y;
    currentRotationAngle = Mathf.LerpAngle (currentRotationAngle, wantedRotationAngle, rotationDamping * Time.deltaTime);
    currentHeight = Mathf.Lerp (currentHeight, wantedHeight, heightDamping * Time.deltaTime);
    currentRotation = Quaternion.Euler (0, currentRotationAngle, 0);

    transform.position = target.position;
    
    switch(orientation){
    	case "across":
		    
		   if(followRot)  transform.position -= currentRotation * Vector3.forward * distance * smooth;
		   else {
				transform.position.z -= Mathf.Sin(Time.time*rotationSpeed) * camDistance;
				transform.position.x -= Mathf.Cos(Time.time*rotationSpeed) * camDistance;
		   }
		   break;
		case "down":
		 	transform.position.y += 10;
			break;
		case "up":
		 	transform.position.y -= 10;
			break;
    }
    
    transform.LookAt (target);
}