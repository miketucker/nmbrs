Shader "Flat" {
	Properties {
	  _Color ("Main Color", Color) = (1,1,1,1) 
	}

	SubShader { 


		Pass { 

	        Lighting Off
	        Color [_Color]
			Fog { Mode Off } 
			ZWrite On
		} 
	}
}