using UnityEngine;
using System.Collections;

public class RagTestForce : MonoBehaviour 
{
	public Transform spine2;
	public float yForce = 8000.0f;

	
	// Update is called once per frame
	void Update () 
	{
		if (Input.GetButton("Fire1"))
		{
			spine2.rigidbody.AddForce(Vector3.up * yForce * Time.deltaTime);
		}
	
	}
}
