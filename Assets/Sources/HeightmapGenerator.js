// This script is placed in public domain. The author takes no responsibility for any possible harm.
var maxHeight:float = 5;
var tickDiff:float = 2.0f;
var lacunarity:float = 6.18;
var h:float = 0.69;
var octaves:float = 8.379;
var offset:float = 0.75;
var decayPerlin:float = .8;
var avgLevel:float = .5;
var levelDecay:float = .5;

var heightMap : Texture2D;
var material : Material;
var size:Vector3 = Vector3(100, 30, 100);
var scale:float = 0.09;
var centerSine:float = 0.2;
var sinePercent:float = 0.5;

private var verts:Vector3[];
private var mesh : Mesh ;

private var currentRow:int = 0;

private var width : int = 100;
private var height : int = 100;

private	var vertices = new Vector3[height * width];
private	var uv = new Vector2[height * width];
private var triangles:int[];

private var hw:int;
private var hh:int;
private var newColors:Color[];
private var fractal : FractalNoise;
private var perlin : Perlin;

private var avgs:float[];

function Start ()
{
	hw = width;
	hh = height;

	avgs = new float[width];
	newColors = new Color[width];

	perlin = new Perlin();
	fractal = new FractalNoise(h, lacunarity, octaves, perlin);

	mesh = GetComponent(MeshFilter).mesh;
	GenerateUV();

	triangles = new int[(height - 1) * (width - 1) * 6];
	GenerateHeightmap();
}

function Update(){
	
	UpdateTexture();

		GenerateUV();
		GenerateHeightmap();
}


function UpdateTexture(){
	var colors:Color[] = heightMap.GetPixels (0, 0, hw , hh);
	// newColors = heightMap.GetPixels (0, hh-1, width , 1);
	var i:int;
	var r:float;

	for(i=0; i < colors.length;i++){
		if(perlin.Noise(i,i/width) > decayPerlin)
			r = Mathf.Clamp(colors[i].r - levelDecay,0,1);
		else 
			r = colors[i].r;
		colors[i] = Color(r,r,r,r);
	}

	heightMap.SetPixels(0,1,hw,hh-1,colors);
	var t:float = Time.time + 10.0f;
	for(i =0; i < width;i++){
		r = fractal.HybridMultifractal(i*scale + t, tickDiff * i * scale + t, offset);
		avgs[i] += (OSCReceiver.GetLevel(1.0*i/width) - avgs[i]) * avgLevel;
		r *= avgs[i];
		newColors[i] = Color(r,r,r);
	}
	heightMap.SetPixels(0,0,width,1,newColors);

}


function GenerateUV(){
	var y = 0;
	var x = 0;

	// Build vertices and UVs

	
	var uvScale = Vector2 (1.0 / (width - 1), 1.0 / (height - 1));
	var sizeScale = Vector3 (size.x / (width - 1), size.y, size.z / (height - 1));
	var p:float;
	for (y=0;y<height;y++)
	{
		for (x=0;x<width;x++)
		{
			var pixelHeight:float = heightMap.GetPixel(x,y).r * maxHeight;
			p = x * sinePercent;
			pixelHeight-=  Mathf.Sin(p * Mathf.PI) * centerSine;
			var vertex = Vector3 (x, pixelHeight, y);
			vertices[y*width + x] = Vector3.Scale(sizeScale, vertex);
			uv[y*width + x] = Vector2.Scale(Vector2 (x, y), uvScale);
		}
	}
}

function GenerateHeightmap ()
{


	
	// Assign them to the mesh
	mesh.vertices = vertices;
	mesh.uv = uv;

	// Build triangle indices: 3 indices into vertex array for each triangle

	var index = 0;
	for (y=0;y<height-1;y++)
	{
		for (x=0;x<width-1;x++)
		{
			// For each grid cell output two triangles
			triangles[index++] = (y     * width) + x;
			triangles[index++] = ((y+1) * width) + x;
			triangles[index++] = (y     * width) + x + 1;

			triangles[index++] = ((y+1) * width) + x;
			triangles[index++] = ((y+1) * width) + x + 1;
			triangles[index++] = (y     * width) + x + 1;
		}
	}
	// And assign them to the mesh
	mesh.triangles = triangles;
		
	// Auto-calculate vertex normals from the mesh
	mesh.RecalculateNormals();

}
