public var minAmp:float = .5;
var amp:float = 3.0f;
var bounceAmp:float = 0.1;
var maxHeight:float = 5;
var tickDiff:float = 2.0f;
var lacunarity:float = 6.18;
var h:float = 0.69;
var octaves:float = 8.379;
var offset:float = 0.75;
var decayPerlin:float = .8;
var avgLevel:float = .5;
var levelDecay:float = .5;

var heightMap : Texture2D;
var material : Material;
var size:Vector3 = Vector3(100, 30, 100);
var scale:float = 0.09;
var centerSine:float = 0.2;
var sinePercent:float = 0.5;

private var verts:Vector3[];
private var mesh : Mesh ;

private var currentRow:int = 0;

private var width : int = 100;
private var height : int = 100;

private	var vertices = new Vector3[height * width];
private	var uv = new Vector2[height * width];
private var triangles:int[];

private var hw:int;
private var hh:int;
private var newColors:Color[];
private var fractal : FractalNoise;
private var perlin : Perlin;

private var avgs:Array = [];
private var nums:Array = [];

function Start ()
{


	perlin = new Perlin();
	fractal = new FractalNoise(h, lacunarity, octaves, perlin);

	var ar:float[];
	var j:int = 0;
	var t:float;
	for(var i:int =0; i < height; i++){
		ar = new float[width];
		t = Time.time + 10.0f + i;

		for(j=0; j < width;j++){
			ar[j] = fractal.HybridMultifractal(j*scale + t, tickDiff * j * scale + t, offset);
		}
		nums.push(ar);	
		avgs.push(new float[width]);	

	}

	hw = width;
	hh = height;

	newColors = new Color[width];

	mesh = GetComponent(MeshFilter).mesh;
	GenerateUV();

	triangles = new int[(height - 1) * (width - 1) * 6];
	GenerateHeightmap();
}

function Update(){
	
	UpdateTexture();
	GenerateUV();
	GenerateHeightmap();
}


function UpdateTexture(){
	// var colors:Color[] = heightMap.GetPixels (0, 0, hw , hh);
	// newColors = heightMap.GetPixels (0, hh-1, width , 1);
	var i:int;
	var r:float;

	// for(i=0; i < colors.length;i++){
	// 	if(perlin.Noise(i,i/width) > decayPerlin)
	// 		r = Mathf.Clamp(colors[i].r - levelDecay,0,1);
	// 	else 
	// 		r = colors[i].r;
	// 	colors[i] = Color(r,r,r,r);

	// }

	var t:float = Time.time + 10.0f;

	var row:float[] = new float[width];

	for(i =0; i < width;i++){
		r = fractal.HybridMultifractal(i*scale + t, tickDiff * i * scale + t, offset);
		// avgs[i] += (OSCReceiver.GetLevel(1.0*i/width) - avgs[i]) * avgLevel;
		// r *= avgs[i];


		row[i] = r * ((OSCReceiver.GetLevel(1.0*i/width) + minAmp) * amp + .2);
		// row[i] = r * amp;

		// newColors[i] = Color(r,r,r);
	}

	nums.shift();
	nums.push(row);

}


function GenerateUV(){
	var y = 0;
	var x = 0;

	// Build vertices and UVs

	
	var uvScale = Vector2 (1.0 / (width - 1), 1.0 / (height - 1));
	var sizeScale = Vector3 (size.x / (width - 1), size.y, size.z / (height - 1));
	var p:float;
	var row:float[];
	var k:int =0;
	var avg:float[];
	for (y=0;y<height;y++)
	{
		avg = avgs[y];
		row = nums[y];
		for (x=0;x<width;x++)
		{
			// row[x] += row[x] * (OSCReceiver.messages[k] + .5) * bounceAmp;
			avg[x] += (OSCReceiver.GetLevel(1.0*x/width) - avg[x]) * bounceAmp;


			var pixelHeight:float = row[x] * avg[x];
			p = x * sinePercent;
			pixelHeight-=  Mathf.Sin(p * Mathf.PI) * centerSine;
			var vertex = Vector3 (x, pixelHeight, y);
			vertices[y*width + x] = Vector3.Scale(sizeScale, vertex);
			uv[y*width + x] = Vector2.Scale(Vector2 (x, y), uvScale);
			k++;
			if(k>3) k =0;
		}
	}
}

function GenerateHeightmap ()
{


	
	// Assign them to the mesh
	mesh.vertices = vertices;
	mesh.uv = uv;

	// Build triangle indices: 3 indices into vertex array for each triangle

	var index = 0;
	for (y=0;y<height-1;y++)
	{
		for (x=0;x<width-1;x++)
		{
			// For each grid cell output two triangles
			triangles[index++] = (y     * width) + x;
			triangles[index++] = ((y+1) * width) + x;
			triangles[index++] = (y     * width) + x + 1;

			triangles[index++] = ((y+1) * width) + x;
			triangles[index++] = ((y+1) * width) + x + 1;
			triangles[index++] = (y     * width) + x + 1;
		}
	}
	// And assign them to the mesh
	mesh.triangles = triangles;
		
	// Auto-calculate vertex normals from the mesh
	mesh.RecalculateNormals();

}
