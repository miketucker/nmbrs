using UnityEngine;
using System.Collections;

public class CamMove : MonoBehaviour {
	public float resetZ = 1500.0f;
	private Vector3 startPos;
	public float speed = 1.0f;
	// Use this for initialization
	void Start () {
		startPos = transform.position;
	}
	
	// Update is called once per frame
	void Update () {
		transform.position += Vector3.forward * speed * Time.deltaTime;
		if(transform.position.z > resetZ) transform.position = startPos;
	}
}
