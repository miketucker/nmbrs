using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DrivingScene : MonoBehaviour {
	public GUIText debug;
	public GameObject[] prefabs;	
	public Material[] mats;
	public Transform target;
	public float forwardAmount = 100;
	// Use this for initialization

	private bool isRunning = false;


	void Start () {

		StartCoroutine("MakeClones");
		isRunning = true;
		// StartCoroutine("MakeText");
	}

	void Log(string str)
	{
		debug.text = str + "\r\n" + debug.text;
	}


	IEnumerator MakeClones()
	{
		yield return new WaitForSeconds(.5f);
		while(true)
		{
				GameObject go = GameObject.Instantiate(prefabs[Random.Range(0,prefabs.Length)]) as GameObject;
				go.SetActive(true);
				go.renderer.material = mats[Random.Range(0,mats.Length)];
				MeshCollider mc = go.GetComponent<MeshCollider>();
				if(mc==null)
				{
					go.GetComponent<MeshCollider>();
					mc.convex = true;
				}
				go.AddComponent<Rigidbody>();
				go.transform.position = target.position + Vector3.forward * forwardAmount;
				go.transform.localEulerAngles += new Vector3(0,Random.Range(-45.0f,45.0f),0);
			
			yield return new WaitForSeconds(5.0f);
		}


	}
}
