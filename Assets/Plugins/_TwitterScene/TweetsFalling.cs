using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TweetsFalling : MonoBehaviour {
	public GUIText debug;
	public GameObject prefab;	
	public Transform target;
	public float forwardAmount = 100;
	public float frequency = 20.0f;
	public TwitterFeed feed;
	public Material[] materials;
	public PhysicMaterial physMat;
	public string instructionTxt = "Send us your message by tweeting to #nmbrs";
	private List<string> tweetQueue = new List<string>();
	private List<string> tweetsPlayed = new List<string>();
	// Use this for initialization

	private bool isRunning = false;


	void Start () {
		AddInstructionTweet();
		// StartCoroutine("MakeText");
	}

	void Log(string str)
	{
		if(debug != null) debug.text = str + "\r\n" + debug.text;
	}

	public void TweetsLoaded()
	{

		AddNewTweets(feed.tweets);
		Log("tweets loaded received");

		if(!isRunning)
		{
			StartCoroutine("MakeText");
			isRunning = true;
		}
	}

	private void AddNewTweets(List<string> tweets)
	{

		Log("add new tweets "+tweets.Count);

		foreach(string tweet in tweets)
		{

			int i = tweetsPlayed.FindIndex(delegate (string s) { return s == tweet; });
			Log("find played: "+i);
			if(i >= 0) continue;

			i = tweetQueue.FindIndex(delegate (string s) { return s == tweet; });
			Log("find queued: "+i);
			if(i == -1)
			{
				Log("add new tweet: "+tweet);
				tweetQueue.Add(tweet);
			}
		}
	}

	IEnumerator MakeText()
	{
		yield return new WaitForSeconds(.5f);
		while(true)
		{
			if(tweetQueue.Count > 0)
			{
				MakePrefab(tweetQueue[0]);
				tweetQueue.RemoveAt(0);
				if(tweetQueue.Count > 0) tweetsPlayed.Add(tweetQueue[0]);
			}
			yield return new WaitForSeconds(frequency);
		}
	}

	public void AddInstructionTweet()
	{
		MakePrefab(instructionTxt);
	}


	private void MakePrefab(string text)
	{
		GameObject go = GameObject.Instantiate(prefab) as GameObject;
				
		Vector2 rc = Random.insideUnitCircle * 5;
		go.transform.position = new Vector3(rc.x,15.0f,rc.y);
		TTFText txt = go.GetComponent<TTFText>();
		txt.Text = text;

		go.renderer.material = materials[Random.Range(0,materials.Length)];


		// mc.convex = true;

		Rigidbody r = go.AddComponent<Rigidbody>();
		r.angularVelocity = new Vector3(Random.Range(-1.0f,1.0f),Random.Range(-1.0f,1.0f),Random.Range(-1.0f,1.0f));
		r.drag = 4.0f;
		r.angularDrag = 0.1f;
		r.useGravity = true;
		StartCoroutine(NextFrame(go));
	}

	IEnumerator NextFrame(GameObject go)
	{
		yield return new WaitForSeconds(1.0f);
		go.AddComponent<BoxCollider>();
		BoxCollider bc = go.GetComponent<BoxCollider>();
		bc.material = physMat;

		bc.center -= bc.center;
	}
}
