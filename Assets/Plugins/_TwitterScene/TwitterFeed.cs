using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TwitterFeed : MonoBehaviour {

	public string feedUrl = "http://mike-tucker.com/numbers/twitter.php?u=miketucker";
	public float refreshSeconds = 60.0f;
	public GUIText txt;

	public List<string> tweets = new List<string>();

	// Use this for initialization
	void Start () {
		StartCoroutine("LoadTwitter");

	}

	void Log(string str)
	{
		if(txt != null ) txt.text = str + "\r\n" + txt.text;
		Debug.Log(str);
	}

	IEnumerator LoadTwitter()
	{
		while(true)
		{
			WWW data = new WWW(feedUrl+"&unique="+Random.Range(1,19999999));
			Log("load feed: "+feedUrl);
			yield return data;
			LoadJson(data.text);
			SendMessage("TweetsLoaded");

			yield return new WaitForSeconds(refreshSeconds);
		}
	}
	
	// Update is called once per frame
	void LoadJson(string str) {
		JSONObject j = new JSONObject(str);
		AccessData(j);
	}

	void AccessData(JSONObject obj){
		switch(obj.type){
			case JSONObject.Type.OBJECT:
				for(int i = 0; i < obj.list.Count; i++){
					if(obj.keys.Count > i)
					{
						string key = (string)obj.keys[i];
						JSONObject j = (JSONObject)obj.list[i];
						
						if(key=="text")
						{
							Log("got a tweet: "+j.str);
							tweets.Add(j.str);
						}
					}
					
					// AccessData(j);
				}
				break;
			case JSONObject.Type.ARRAY:
				foreach(JSONObject j in obj.list){
					AccessData(j);
				}
				break;
			case JSONObject.Type.STRING:
				Debug.Log(obj.str);
				break;
			case JSONObject.Type.NUMBER:
				Debug.Log(obj.n);
				break;
			case JSONObject.Type.BOOL:
				Debug.Log(obj.b);
				break;
			case JSONObject.Type.NULL:
				Debug.Log("NULL");
				break;
	 
		}
	}
}
