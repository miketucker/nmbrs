//  Unity TTF Text
//  Copyrights 2011-2012 ComputerDreams.org O. Blanc & B. Nouvel
//  All infos related to this software at http://ttftext.computerdreams.org/
//   

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
#if UNITY_STANDALONE_WIN || UNITY_STANDALONE_OSX || UNITY_EDITOR
using TTF = FTSharp;
#else
public class TTF {
	public class Font : System.Object {}
}
#endif

// TODO: Do AppendMetricInfo (Not only get metric info)


public static class TTFTextInternal {
	//byte [] b=System.Convert.FromBase64String()
	public static string open_markup="<@";
	public static string close_markup="@>";
	
	
#region Line Layouts
	
	[System.Serializable]
    public class LineLayout 
    {
		private const int default_alloc=8;
				
        private string linetext;    // text 
		public string line {get { return linetext;}  }
		
        public float hspacing; // spacing to be used for characters
        public float offset;   // offset for the beginning of the line
		public Vector3 advancedir;
        public float advancelen;  // advance for the whole line
		public Vector3 advance { get { return advancedir * advancelen ;}}
		// NEW FROM 1.0.15
		public TTFText tm;
		public TTFText.ParagraphAlignmentEnum align;
		public float linewidth;
		public float [] charadvances;  // for each character this is the ideal advance
		public float [] charheights;
		public float [] charpositions; // we shall allow user to set up each character individually
		public int lineno; // <- metadata 
		// NEW FROM 1.0.16
		//public TTFTextStyle [] charstyle; // each character may have a different style...
		public int [] charstyleindex;
		// NEW FROM 1.1
		public float prevadvance=0;
				
		public float CharSumAdvance(int c) {
			float s=prevadvance;
			for (int i=0; i<c;i++) {s+=charadvances[c];}
			return s;
		}
		
		public TTFTextStyle GetCharStyle(int pos) {
#if TTFTEXT_LITE
			if (!tm.DemoMode) {return tm.InitTextStyle;}			
#endif			
			if (charstyleindex[pos]==-1) {			
				Debug.Log(pos);
			}
			return tm.UsedStyles[charstyleindex[pos]];
		}
		
	
		
		float Xmin0 = 0; // leftmost boundary for the first glyph of this line
		float XmaxN = 0; // rightmost boundary for the last glyph of this line
		
		public float MaxCharacterSize{ get {
			float msz=0;
			for (int i=0;i<linetext.Length;i++) {
				//Debug.Log(System.String.Format("{0} {1} {2} {3}",i,msz,GetCharStyle(i).Size,charheights[i]));
				if (/*(charstyleindex[i]!=-1)&&*/
				     (linetext[i]!=' ')
					 &&(Mathf.Abs(GetCharStyle(i).Size*charheights[i])>msz)) {
//					Debug.Log("<="+Mathf.Abs(GetCharStyle(i).Size*charheights[i]));
					msz=Mathf.Abs(GetCharStyle(i).Size*charheights[i]);
				}
				/*	
				else {
						
						Debug.Log(
							System.String.Format("{0} {1} {2}",
							(GetCharStyle(i)!=null),
							(linetext[i]!=' '),
							(Mathf.Abs(GetCharStyle(i).Size*charheights[i])>msz)));
				}
				*/
			}
			return msz;
		}
		}

		
		public void SetCharStyle(int f,int t,int stidx) {
			for (int i=f;i<t;i++) {
				while (i>=charadvances.Length) {
					System.Array.Resize<float>(ref charadvances,charadvances.Length*2);
				}
				while (i>=charpositions.Length) {
					System.Array.Resize<float>(ref charpositions,charpositions.Length*2);
				}				
				while (i>=charstyleindex.Length) {					
					System.Array.Resize<int>(ref charstyleindex,charstyleindex.Length*2);
					for (int j=t;j<charstyleindex.Length;j++) charstyleindex[j]=-1;
				}
				while (i>=charheights.Length) {					
					System.Array.Resize<float>(ref charheights,charheights.Length*2);
				}
				
				charstyleindex[i]=stidx;
			}
		}
		
		public void AppendText(string s) {
			if (s.Length==0) return;
			int l0=linetext.Length;			
			linetext+=s;
#if TTFTEXT_LITE						
			if (tm.DemoMode) {
#endif				
			SetCharStyle(l0,l0+s.Length,tm.currentStyleIdx);
#if TTFTEXT_LITE						
			}
			else {
			  SetCharStyle(l0,l0+s.Length,0);	
			}
#endif				
			
			// UPDATE THE METRIC INFO
			object f=null;
			object parameters=null; ;
			int fp=0;
			string currentfontid="";
			
			f=tm.InitTextStyle.GetFont(ref fp, ref currentfontid, ref parameters);
			
			if (f==null) {
							throw new System.Exception("Font not found :"+fp+"/"+tm.InitTextStyle.GetFontProviderFontId(fp));
			}
				
			for(int i=0;i<s.Length;i++) {
					TTFTextStyle cttfstyle=GetCharStyle(l0+i);					
					
					if ((cttfstyle!=null)&&(currentfontid!=cttfstyle.GetFontProviderFontIdD(fp))) {
						TTFTextFontProvider.font_providers[fp].DisposeFont(f);					    
						f=cttfstyle.GetFont(ref fp, ref currentfontid, ref parameters);

						if (f==null) {
							throw new System.Exception("Font not found :"+fp+"/"+tm.InitTextStyle.GetFontProviderFontId(fp));
						}
					}
					
				    
					charadvances[l0+i] = TTFTextFontProvider.font_providers[fp].GetAdvance(parameters,f,s[i]).x*cttfstyle.Size;
					charheights[l0+i]=TTFTextFontProvider.font_providers[fp].GetHeight(parameters,f);
				
					TTFTextOutline o=TTFTextFontProvider.font_providers[fp].GetGlyphOutline(parameters,f,s[i]);
					o.Rescale(cttfstyle.Size);				
					if (l0+i == 0) { Xmin0 = o.Min.x; }
					if (l0+i == line.Length - 1) { XmaxN = o.Max.x; }
			}
			
			TTFTextFontProvider.font_providers[fp].DisposeFont(f);
			
			/*
			if (tm.EmbedCharset) {
#if !TTFTEXT_LITE											
				TTFTextFontStore tfs=GameObject.Find("/TTFText Font Store").GetComponent<TTFTextFontStore>();
				TTFTextFontStoreFont tfsf=tfs.GetFont(tm.FontId);				
				if (tfsf == null || tfsf.charset == null) { 
					Debug.LogError("tfsf = null : font id="+tm.FontId); 
					return;
				}
				
				for (int i = 0; i < s.Length; i++) {
					char c = s[i];
					TTFTextStyle cttfstyle=GetCharStyle(l0+i);					
					
					if ((cttfstyle!=null)&&(tfsf.fontid!=cttfstyle.FontId)) {
						TTFTextFontStoreFont ptfsf=tfsf;
						tfsf=tfs.GetFont(cttfstyle.FontId);							
						if (tfsf==null) {
							Debug.LogWarning(System.String.Format("Can't find font {0} in Font Store",cttfstyle.FontId));
						
						if (Application.isEditor) {
							tfsf=tfs.EnsureFont(cttfstyle.FontId);
							if (tfsf.charset==null) {
								tfsf.BuildCharSet(cttfstyle.FontId);
								Debug.Log("Markup currently make fonts more difficult to track... Fonts may leak in the font store.");
							}
							if (tfsf==null) {
								Debug.LogWarning("Can't instantiate font %s in Font Store");
							}							
						}
						else {
							Debug.Log("Trying a fallback");
							tfsf=ptfsf;
						}
						}
					}
					

					
					int idx = tfsf.additionalChar.IndexOf(s[i]);
					TTFTextOutline o;					
					if (idx != -1) {
						o = new TTFTextOutline(tfsf.addCharset[idx]);			        
					} else {						
						idx = (int) c;						
						if (idx < 0 || idx >= tfsf.charset.Length) { // fallback on space char
							idx = (int) ' ';
						}
						o = new TTFTextOutline(tfsf.charset[idx]);
					}
					o.Rescale(cttfstyle.Size);
					charadvances[l0+i] = o.advance.x;	
					charheights[l0+i]=tfsf.height;
					if ((l0+i) == 0) { Xmin0 = o.Min.x; }
					if ((l0+i) == line.Length - 1) { XmaxN = o.Max.x; }
				}
#endif			
			} else {
#if UNITY_STANDALONE_WIN || UNITY_STANDALONE_OSX || UNITY_EDITOR				
				TTF.Font f = TryOpenFont(tm.InitTextStyle, 1);				
				if (f==null) {
							throw new System.Exception("Font not found :"+tm.InitTextStyle.FontId);
				}
				
				for(int i=0;i<s.Length;i++) {
					TTFTextStyle cttfstyle=GetCharStyle(l0+i);					
					
					if ((cttfstyle!=null)&&(f.Name!=cttfstyle.FontId)) {
						f.Dispose();
						f = TryOpenFont(cttfstyle.FontId, cttfstyle.OrientationReversed, cttfstyle.runtimeFontFallback);
						if (f==null) {
							new System.Exception("Font not found :"+cttfstyle.FontId);
						}
					}
					
					TTF.Outline.Point xadvance;
					TTF.Outline ol=f.GetGlyphOutline(s[i],out xadvance,true,0,1);					
					TTFTextOutline o=new TTFTextOutline(ol,xadvance,cttfstyle.OrientationReversed);					
					o.Rescale(cttfstyle.Size);
					charadvances[l0+i] = o.advance.x;
					charheights[l0+i]=f.Height;
					if (l0+i == 0) { Xmin0 = o.Min.x; }
					if (l0+i == line.Length - 1) { XmaxN = o.Max.x; }
				}
				f.Dispose();
#endif				
			}
			*/
			
			
			// can happen when the first or last char is non printable, like a space for example
			if (float.IsNaN(XmaxN) || float.IsInfinity(XmaxN)) { XmaxN = 0; }
			if (float.IsNaN(Xmin0) || float.IsInfinity(Xmin0)) { Xmin0 = 0; }
		
		}
		
		public LineLayout(string l, TTFText atm, bool initialize) {
			offset=0;
			linetext="";
			hspacing=atm.Hspacing;		
		    tm=atm;
			linewidth=tm.CurrentTextStyle.LineWidth;
			advancedir=Vector3.right;
			charadvances=new float[default_alloc];  // for each character this is the ideal advance			
		    charpositions=new float[default_alloc]; // we shall allow user to set up each character individually
			charheights=new float[default_alloc]; 
			charstyleindex=new int[default_alloc];
			for (int j=0;j<default_alloc;j++) {charstyleindex[j]=-1;}				
				

			advancelen=0;
			lineno=0;
			align=tm.CurrentTextStyle.ParagraphAlignment;
			AppendText(l);
			//ComputeMetricInfo();
			if (initialize) {
		    	ComputeCharacterPositions();
			}
		}
		
		public void RewindLine(string s) {
			if (linetext.StartsWith(s)) {
				linetext=s;
			}
		}
		// extra incomprssible width from the left of first char, and the right of last one
		public float BoundsWidth { 
		  get {
			if (line.Length == 0) { return 0; }
			return XmaxN - Xmin0;
		  }
		}
		
		
		// inside width = sum of advances between chars (all advances but the last one)
		public float SumNM1AdvanceAmounts() { // the sum of the advances
			float w = 0;
			for (int i = 0; i < line.Length - 1; ++i) {
				w += charadvances[i];
			}
			return w;
		}
		
		public float AdvanceLastCharacter() {
			if (line.Length == 0) { return 0; }
			return charadvances[line.Length - 1];
		}
		

		// THIS IS THE AMOUNT OF TEXT INSIDE OF THE LINE

		public float AdvanceBasedLineWidth() {
			return AdvanceLastCharacter() + SumNM1AdvanceAmounts();
		}
		
		
		public float GetDefaultLineWidth(float hspacing) {
			
		    float outside;
			if (tm.HSpacingMode == TTFText.HSpacingModeEnum.GlyphBoundaries) {
				outside = BoundsWidth;
			} else {
				outside = AdvanceLastCharacter();
			}
			
			return SumNM1AdvanceAmounts() * tm.Hspacing + outside;
		}
		
		
		
		

		// THIS IS WIDTH OF THE LINE WITH ITS LAYOUT
		
		public float GetActualWidthBoundariesBased() {
			if (line.Length == 0) { return 0; }
			return charpositions[line.Length - 1] - charpositions[0] + BoundsWidth;
		}
		
		public float GetActualWidthAdvanceBased() {
			if (line.Length == 0) { return 0; }
			//Debug.Log(System.String.Format("P : {0} {1} {2}",charpositions[linetext.Length - 1] , charpositions[0] , AdvanceLastCharacter()));
			return charpositions[linetext.Length - 1] - charpositions[0] + AdvanceLastCharacter();
		}
		
		public float GetActualLinewidth() {			
			float f=0;
			switch(tm.HSpacingMode) {
			case TTFText.HSpacingModeEnum.GlyphBoundaries:
				f= GetActualWidthBoundariesBased();
				break;
			case TTFText.HSpacingModeEnum.GlyphAdvance:			
				f= GetActualWidthAdvanceBased();
				break;
			}
			return f;
		}
			
		
		public void ComputeCharacterPositionsBoundaryBased() {
			if (tm.HSpacingMode != TTFText.HSpacingModeEnum.GlyphBoundaries) {
				Debug.LogError("Hspacing = " + tm.CurrentTextStyle.HSpacingMode);
			}
			
			float defW = BoundsWidth + SumNM1AdvanceAmounts();
			float Inside = SumNM1AdvanceAmounts();
			float Xtra = linewidth -offset- defW;
		
#if TTFTEXT_LITE        
		if (tm.DemoMode) {	
#endif			
			
			if (tm.LayoutMode == TTFText.LayoutModeEnum.No // One line mode
		    || ((align!=TTFText.ParagraphAlignmentEnum.Justified ) 
			  && (align!=TTFText.ParagraphAlignmentEnum.FullyJustified))) { // do not justify
				
				//Xtra = (tm.Hspacing -1) * defW;
				Xtra = (tm.CurrentTextStyle.Hspacing -1) * Inside;
			}
#if TTFTEXT_LITE										
		}
		else {
				Xtra = (tm.CurrentTextStyle.Hspacing -1) * Inside;
		}			
#endif			
			
			// Xtra = XtraSpace + XtraAdd + XtraMul
			float XtraSpace = 0;
			float XtraAdd = 0;
			float XtraMul = 0;			
			
			int nspaces = 0;
			foreach (char c in line) {
				if (c == ' ') { ++nspaces; }
			}
			
			float addSpace = 0;
			if (nspaces == 0) {				
				XtraSpace = 0;
				XtraAdd = tm.CurrentTextStyle.HSpacingMultFactor * Xtra;
				XtraMul = (1 - tm.CurrentTextStyle.HSpacingMultFactor) * Xtra;
					
			} else {				
				XtraSpace = tm.CurrentTextStyle.WordSpacingFactor * Xtra;
				addSpace = XtraSpace / nspaces;
				XtraAdd = tm.CurrentTextStyle.HSpacingMultFactor * (1 - tm.CurrentTextStyle.WordSpacingFactor) * Xtra;
				XtraMul = (1 - tm.CurrentTextStyle.HSpacingMultFactor) * (1 - tm.CurrentTextStyle.WordSpacingFactor) * Xtra;
			}
			
			
			float addq = 0;
			float mulf = 1;
			
			if (line.Length > 1) { addq = XtraAdd / (line.Length - 1); }				
			if (Inside != 0) { mulf = (Inside + XtraMul) / Inside; }
			
			// change offset so that leftmost outline point is at coordinate 0			
			float pos = - Xmin0;
			for (int i = 0; i < line.Length; i++) {				
				charpositions[i] = pos;				
				pos += charadvances[i] * mulf + addq;				
				if (line[i] == ' ') { pos += addSpace; }
			}
			
			advancelen = pos;
		}
		
		public void ComputeCharacterPositionsAdvanceBased() {		
			if (tm.HSpacingMode != TTFText.HSpacingModeEnum.GlyphAdvance) {
				Debug.LogError("Hspacing = " + tm.HSpacingMode);
			}			
			// Advance Simple
			
			float defW = AdvanceBasedLineWidth();
			float Inside = SumNM1AdvanceAmounts();
			float Xtra = linewidth -offset- defW;
		
#if TTFTEXT_LITE        
		if (tm.DemoMode) {	
#endif			
			
			if (tm.LayoutMode == TTFText.LayoutModeEnum.No // One line mode
				|| ((align!=TTFText.ParagraphAlignmentEnum.Justified ) 
				     && (align!=TTFText.ParagraphAlignmentEnum.FullyJustified))) { // do not justify				
				// => Xtra = (hspacing - 1) * Inside 
				Xtra = (tm.CurrentTextStyle.Hspacing-1) * Inside;
			}
#if TTFTEXT_LITE											
			}
			else  {
					Xtra = (tm.CurrentTextStyle.Hspacing-1) * Inside;
			}
#endif			
			
			int nspaces = 0;
			// Xtra = XtraSpace + XtraAdd + XtraMul
			float XtraSpace = 0;
			float XtraAdd = 0;
			float XtraMul = 0;
			
			
			foreach (char c in line) {
				if (c == ' ') { ++nspaces; }
			}
			
			float addSpace = 0;
			if (nspaces == 0) {
				XtraSpace = 0;
				XtraAdd = tm.CurrentTextStyle.HSpacingMultFactor * Xtra;
				XtraMul = (1 - tm.CurrentTextStyle.HSpacingMultFactor) * Xtra;
					
			} else {				
				XtraSpace = tm.CurrentTextStyle.WordSpacingFactor * Xtra;
				addSpace = XtraSpace / nspaces;
				XtraAdd = tm.CurrentTextStyle.HSpacingMultFactor       * (1 - tm.CurrentTextStyle.WordSpacingFactor) * Xtra;
				XtraMul = (1 - tm.CurrentTextStyle.HSpacingMultFactor) * (1 - tm.CurrentTextStyle.WordSpacingFactor) * Xtra;
			}
			
			float addq = 0;
			float mulf = 1;
			
			if (line.Length > 1) { addq = XtraAdd / (line.Length - 1); }				
			if (Inside != 0) { mulf = (Inside + XtraMul) / Inside;}
			
			float pos = 0;
			for (int i = 0; i < line.Length; i++) {
				charpositions[i] = pos;
				pos += charadvances[i] * mulf + addq;
				if (line[i] == ' ') { pos += addSpace; }
			}
			
			advancelen = pos;
		}

		
		public void ComputeCharacterPositions() {			
			switch (tm.HSpacingMode) {
			case TTFText.HSpacingModeEnum.GlyphBoundaries:
				ComputeCharacterPositionsBoundaryBased();
				break;
			case TTFText.HSpacingModeEnum.GlyphAdvance:			
				ComputeCharacterPositionsAdvanceBased();
				break;
			}
		}
	}
	
#endregion	
	
	
	
	
	
	
	
	
	
	
	
	
	
#region MAIN_FUNCTION	
	public static Vector3 ObjectTranslationFromBounds(TTFText tm, Bounds bounds) {
		Vector3 tr = Vector3.zero;
				switch (tm.HJust) {
				case TTFText.HJustEnum.Center:
					tr.x = -bounds.center.x; break;
                case TTFText.HJustEnum.Left:
					tr.x = -bounds.min.x; break;
				case TTFText.HJustEnum.Right:
					tr.x = -bounds.max.x;break;
				}
			
				switch (tm.VJust) {
				case TTFText.VJustEnum.Center:
					tr.y = -bounds.center.y; break;
                case TTFText.VJustEnum.Top:
					tr.y = -bounds.max.y; break;
				case TTFText.VJustEnum.Bottom:
					tr.y = -bounds.min.y; break;
				}
				tr.z = - bounds.center.z; 		
				return tr;
	}
	
	// This is the main functon that is used to generate any text mesh
	public static void BuildText(TTFText tm) {
		// OK WE ARE NOT INCREMENTAL NOW... WE DO A COMPLETE RESET 
		// and not progressive
		
			ResetTextMesh(tm);
			tm.ResetUsedStyles();
			tm.CurrentTextStyle=tm.InitTextStyle;
		
		
		if (tm.TokenMode != TTFText.TokenModeEnum.Text) {
#if TTFTEXT_LITE
			if (tm.DemoMode) {
#endif				
			// CASE 1 : WE GENERATE DIFFERENT GAME OBJECTS FOR EACH TOKEN			
			Bounds bounds = new Bounds(Vector3.zero, Vector3.zero);
			Vector3 pos = Vector3.zero;
			int idx = 0;
			int lineno=0;
			float lm=tm.LineSpacingMult;
			float la=tm.LineSpacingAdd;
			
			
				
				
			foreach (LineLayout ll in GenerateTextLayout(tm.Text,  tm)) {
				Bounds bl;
					
				// COMPUTE LINE POSITION	
				ll.lineno=lineno;				
				float msz=Mathf.Abs(ll.MaxCharacterSize); if (msz<=0) msz=tm.Size;
				if ((ll.line.Length>0)&&(ll.charstyleindex[0]!=-1)) {
					lm=ll.GetCharStyle(0).LineSpacingMult;
					la=ll.GetCharStyle(0).LineSpacingAdd;
				}

				if (lineno!=0) {
					pos += new Vector3(0, -  (msz * lm + la), 0);				
				}
					
					
				//	BUILD TEXT
				idx+=BuildTextViaSubObjects(ll,lineSplitters[(int)tm.TokenMode],idx,pos+new Vector3(ll.offset,0,0),tm, out bl);								
					
				// FINALIZE	
				MergeBounds(ref bounds, bl);
				tm.advance=Vector3.Max(tm.advance,ll.advance);
				lineno++;
			}
			
			//if (tm.rebuildLayout) {			
				Vector3 tr = ObjectTranslationFromBounds(tm,bounds); 
				foreach (Transform t in tm.transform){
					t.localPosition += tr;
					if (tm.gameObject) {
					TTFSubtext stx=t.gameObject.GetComponent<TTFSubtext>();
						if (stx!=null) {
							stx.LocalSoftPosition+=tr;
						}
					}
				}
				
			//}
#if TTFTEXT_LITE
			}
#endif				
		} else {			
			// CASE 2 : WE MERGE ALL THE LAYOUT WE HAVE GENERATED IN A SINGLE MESH	
			List<LineLayout> lls= new List<LineLayout>(GenerateTextLayout(tm.Text, tm));
			
			List<CombineInstance []> cis= new List<CombineInstance[]>();			
			int nsubmeshes=ExpectedNumberOfSubmeshes(tm);
			for (int i=0;i<nsubmeshes;i++) {
				cis.Add(new CombineInstance[lls.Count]);
			}
			float lm=tm.LineSpacingMult;
			float la=tm.LineSpacingAdd;			
			int idx=0;
			foreach (LineLayout ll in lls) {				
				Vector3 adv;
				Mesh mesh = new Mesh();
				
				BuildMesh(ref mesh, ll, tm, out adv);
				float msz=Mathf.Abs(ll.MaxCharacterSize); if (msz<=0) msz=tm.Size;
				
				if ((ll.line.Length>0)&&(ll.charstyleindex!=null)&&(ll.charstyleindex[0]!=-1)) {
					lm=ll.GetCharStyle(0).LineSpacingMult;
					la=ll.GetCharStyle(0).LineSpacingAdd;
				}
				
				Vector3 offset = new Vector3(ll.offset, - (msz * lm + la)* idx, 0); // we offseteach line vertically
				//Vector3 offset = new Vector3(ll.offset, - (float) tm.LineSpacing * idx, 0); // we offseteach line vertically
				//Debug.Log("count"+ mesh.vertexCount + "t:" + mesh.triangles.Length+ "s:" + mesh.bounds);
				
				for (int i=0;i<nsubmeshes;i++) { 
					cis[i][idx].mesh=mesh; 
					cis[i][idx].subMeshIndex=i;
					cis[i][idx].transform=Matrix4x4.TRS(offset,Quaternion.identity,new Vector3(1,1,1));					
				}
				idx++;								   
				tm.advance=Vector3.Max(tm.advance,ll.advance);
		    }
			
			if (tm.mesh==null) { tm.mesh=new Mesh();}			
			tm.mesh.Clear();
			
			CombineInstance []tci=new CombineInstance[nsubmeshes];
			for (int i=0;i<nsubmeshes;i++) {
				tci[i].mesh=new Mesh();
				tci[i].mesh.CombineMeshes(cis[i],true,true);
				tci[i].subMeshIndex=0;
				tci[i].transform=Matrix4x4.identity;
			}
			tm.mesh.CombineMeshes(tci,false,false);	
			
			// Free intermediate meshes
			for (int i = 0; i < nsubmeshes; ++i) {
				foreach (CombineInstance c in cis[i]) {
					DestroyObj(c.mesh);
				}
				DestroyObj(tci[i].mesh);
			}
			
			tm.mesh.name="TTF Text AutoGenerated";			
			tm.mesh.RecalculateBounds();
			
			Vector3 tr = ObjectTranslationFromBounds(tm,tm.mesh.bounds); 
			TranslateMesh(tm.mesh, tr);			
			tm.mesh.Optimize(); 
			
			UpdateComponentsWithNewMesh(tm.gameObject, tm.mesh);
		}
	}
   
#endregion
	
		
	
#region LAYOUT_RELATED_FUNCTIONS
	static IEnumerable<LineLayout> EOnePerLine(LineLayout l) { yield return l; } 
	
	static IEnumerable<LineLayout> EOnePerWord(LineLayout l) {

		char[] seps = new char[] { ' ' };
		string[] words = l.line.Split(seps, System.StringSplitOptions.RemoveEmptyEntries);
		
		int p = 0;
		float sa=0;
		foreach (string w in words) {
			
			int wlen = w.Length;
			int p0 = p;
			
			while (l.line[p] != w[0]) p++; // <- skipping spaces : BUG TO BE FIXED !
		    
			LineLayout r=new LineLayout(w, l.tm, false);
			r.prevadvance=l.prevadvance+sa;
			if (wlen>0) {
			  r.linewidth=l.GetCharStyle(p).LineWidth;
			}
			r.charadvances=new float[w.Length];
			r.charpositions=new float[w.Length];
			r.charstyleindex=new int[w.Length];
			
			for (int i = 0; i < wlen; i++) {
				r.charadvances[i] = l.charadvances[p+i];	
				r.charpositions[i] = l.charpositions[p+i] - l.charpositions[p0];		
				r.charstyleindex[i] = l.charstyleindex[p+i];	
			}
			
			if (p + wlen < l.line.Length) {
				r.advancelen = l.charpositions[p+wlen] - l.charpositions[p0];
			} else {
				r.advancelen = l.advancelen-l.charpositions[p0];
			}
			r.lineno=l.lineno;
			yield return r;
			sa+=r.advancelen;
			p += wlen;
		}
	} 
	
	static IEnumerable<LineLayout> EOnePerChar(LineLayout l) {		
			
		int i=0;
		float sa=0;
		foreach (char c in l.line)  {
			LineLayout r=new LineLayout(""+c,l.tm,false);		
			r.charadvances=new float[1];
			r.charpositions=new float[1];												
			r.charstyleindex=new int[1];	
			r.charadvances[0]=l.charadvances[i];	
			r.charpositions[0]=0;	
			r.charstyleindex[0]=l.charstyleindex[i];	
			r.prevadvance=l.prevadvance+sa;
			r.linewidth=l.GetCharStyle(i).LineWidth;
			r.lineno=l.lineno;
			if (i+1<l.line.Length) {
				r.advancelen=l.charpositions[i+1]-l.charpositions[i];
			}
			else {
				r.advancelen=l.advancelen-l.charpositions[i];					
			}
			yield return r;
			sa+=r.advancelen;
			
			i+=1;
		}
	} 
	
	public delegate IEnumerable<LineLayout> LineSplitter(LineLayout l);
	static LineSplitter [] lineSplitters = {EOnePerLine, EOnePerLine,EOnePerWord,EOnePerChar};
	
		
		
	
	
	
	public static IEnumerable<LineLayout> WrapParagraphsSimple(string text,  TTFText tm) {	
		// This function proceeds as follow it generates outline adding words one by one
		// if the line becomes too long for the desired textwidth it starts a new line
		 
		float d;
		int lno=0;
		char[] lseps = new char[] { '\n' };
		char[] wseps = new char[] { ' ' };
		string [] paragraphs=text.Split(lseps);
		
		if (paragraphs.Length == 0) { yield break; }
		float firstlineoffset=0;
		if ((tm.ParagraphAlignment!=TTFText.ParagraphAlignmentEnum.Right)&&(tm.ParagraphAlignment!=TTFText.ParagraphAlignmentEnum.Center)) {
			firstlineoffset=tm.FirstLineOffset;
		}
		
		foreach (string paragraph in paragraphs) {
		
			
		string[] words=null;
		
		if (tm.WordSplitMode==TTFText.WordSplitModeEnum.SpaceBased) {
			words = paragraph.Split(wseps, System.StringSplitOptions.RemoveEmptyEntries);
		}	
		if (tm.WordSplitMode==TTFText.WordSplitModeEnum.Character) {				
			words= new string[paragraph.Length];
			int ci=0;
			foreach (char c in paragraph) {
					words[ci]=""+c;
					ci++;
			}
		}
			
			
		//
		//return new LineLayout(words[0],tm,true);
		if (words.Length==0) {
				yield return new LineLayout("",tm,true);
				if ((tm.ParagraphAlignment!=TTFText.ParagraphAlignmentEnum.Right)&&(tm.ParagraphAlignment!=TTFText.ParagraphAlignmentEnum.Center)) {
					firstlineoffset=tm.FirstLineOffset;
				}
		}
		else {
						
		LineLayout ll = new LineLayout(words[0],tm,true);
	    
		for (int idx = 1; idx < words.Length; ++idx) {			
			string w = words[idx];
			string otext=ll.line;
			if (tm.WordSplitMode==TTFText.WordSplitModeEnum.SpaceBased) {
				ll.AppendText(" " + w);
			} else {
				ll.AppendText(w);
			}
						
				
			d = ll.GetDefaultLineWidth(ll.hspacing);
			
			if (d > (ll.linewidth-firstlineoffset)) {
				ll.RewindLine(otext);
				ll.offset=0;
						
				if (((ll.align != TTFText.ParagraphAlignmentEnum.Right))		
					&& ((ll.align != TTFText.ParagraphAlignmentEnum.Center))) {
					ll.offset=firstlineoffset;		
				}
				ll.ComputeCharacterPositions();
		
                if ((ll.align == TTFText.ParagraphAlignmentEnum.Right))
                {
//					Debug.Log(System.String.Format("{0} {1}",ll.linewidth, ll.GetActualLinewidth()));
					ll.offset = ll.linewidth - ll.GetActualLinewidth();
				}
                else {
					if ((ll.align == TTFText.ParagraphAlignmentEnum.Center))
                	{
						ll.offset = (ll.linewidth-ll.GetActualLinewidth())/2;
					}
					else {
						ll.offset=firstlineoffset;
					}
				}
				
				ll.lineno=lno++;
				yield return ll;
				firstlineoffset=0;
				ll = new LineLayout(w,tm,true);
				
			} 
		}
		
		// last line
        if ((tm.ParagraphAlignment == TTFText.ParagraphAlignmentEnum.Justified)) {
			ll.align=TTFText.ParagraphAlignmentEnum.Left;
		}
		
		ll.offset=0;
				if (((ll.align != TTFText.ParagraphAlignmentEnum.Right))		
					&& ((ll.align != TTFText.ParagraphAlignmentEnum.Center))) {
					ll.offset=firstlineoffset;		
			}
				
			ll.ComputeCharacterPositions();								
        if ((tm.ParagraphAlignment == TTFText.ParagraphAlignmentEnum.Right)) {
			ll.offset = ll.linewidth-ll.GetActualLinewidth();
		}
        else {
		if ((tm.ParagraphAlignment == TTFText.ParagraphAlignmentEnum.Center)) {
			ll.offset = (ll.linewidth-ll.GetActualLinewidth())/2;
		}
					else {
						ll.offset=firstlineoffset;
					}
					
		}
				
		ll.lineno=lno++;
		yield return ll;
		if ((tm.ParagraphAlignment!=TTFText.ParagraphAlignmentEnum.Right)&&(tm.ParagraphAlignment!=TTFText.ParagraphAlignmentEnum.Center)) {
					firstlineoffset=tm.FirstLineOffset;
				}		
			}
		}
	}
	

	
	
	public static IEnumerable<LineLayout> WrapParagraphsExt(string text, TTFText tm) {
		float d;
		int lno=0;
		char[] lseps = new char[] { '\n' };
		char[] wseps = new char[] { ' ' };
		string [] paragraphs=text.Split(lseps);		
		if (paragraphs.Length == 0) { yield break; }
		
		
		float firstlineoffset=0;
		if ((tm.ParagraphAlignment!=TTFText.ParagraphAlignmentEnum.Right)&&(tm.ParagraphAlignment!=TTFText.ParagraphAlignmentEnum.Center)) {
			firstlineoffset=tm.FirstLineOffset;
		}
		
		foreach (string paragraph in paragraphs) {		
			System.Collections.Generic.List<TTFTextScriptTextToken>  ttstts=new System.Collections.Generic.List<TTFTextScriptTextToken>(ParseScriptText(paragraph));	
			
			//string[] words = paragraph.Split(wseps, System.StringSplitOptions.RemoveEmptyEntries);
			int ntexts=0;
			foreach (TTFTextScriptTextToken t in ttstts ) {
				if (!t.is_script) ntexts++;
			}
			
			//return new LineLayout(words[0],tm,true);
			if (ntexts==0) {
				yield return new LineLayout("",tm,true);
				if ((tm.ParagraphAlignment!=TTFText.ParagraphAlignmentEnum.Right)&&(tm.ParagraphAlignment!=TTFText.ParagraphAlignmentEnum.Center)) {
					firstlineoffset=tm.FirstLineOffset;
				}
			} else {
				LineLayout ll = new LineLayout("",tm,true);	    
				//int idx=1;
				foreach (TTFTextScriptTextToken t in ttstts) {			
					if (t.is_script) {
						//EvalScriptText(tm,t.text);		
						EvalEasyMarkUp(tm,t.text);
					} else {
									
					string[] words=null;
		
					if (tm.CurrentTextStyle.WordSplitMode==TTFText.WordSplitModeEnum.SpaceBased) {
						words = t.text.Split(wseps, System.StringSplitOptions.RemoveEmptyEntries);
					}
						
					if (tm.CurrentTextStyle.WordSplitMode==TTFText.WordSplitModeEnum.Character) {				
						words= new string[t.text.Length];
						int ci=0;
						foreach (char c in t.text) {
							words[ci]=""+c;
							ci++;
						}
					}

						
						foreach (string w in words/*t.text.Split(wseps)*/) {
							string otext=ll.line;
							if (otext.Length!=0) {
									if (tm.CurrentTextStyle.WordSplitMode==TTFText.WordSplitModeEnum.SpaceBased) {
										ll.AppendText(' ' + w);
									} else {
										ll.AppendText(w);
									}
							}
							else {
								ll.AppendText( w);								
							}
							
							
							d = ll.GetDefaultLineWidth(ll.hspacing); // TODO: <- THIS IS WRONG !
							// les char advances ne sont plus bases sur les bonnes fontes ..
			
							if (d > (ll.linewidth-firstlineoffset)) {
								ll.RewindLine(otext);
								ll.offset=0;

			
				if (((ll.align != TTFText.ParagraphAlignmentEnum.Right))		
					&& ((ll.align != TTFText.ParagraphAlignmentEnum.Center))) {
					ll.offset=firstlineoffset;		
				}
				ll.ComputeCharacterPositions();
		
                if ((ll.align == TTFText.ParagraphAlignmentEnum.Right))
                {
//					Debug.Log(System.String.Format("{0} {1}",ll.linewidth, ll.GetActualLinewidth()));
					ll.offset = ll.linewidth - ll.GetActualLinewidth();
				}
                else {
					if ((ll.align == TTFText.ParagraphAlignmentEnum.Center))
                	{
						ll.offset = (ll.linewidth-ll.GetActualLinewidth())/2;
					}
				else {
					ll.offset=firstlineoffset;								
				}
									
				}								
								ll.lineno=lno++;
								yield return ll;
								firstlineoffset=0;
								ll = new LineLayout(w,tm,true);
						} 
				//		idx++;
					}
				}
			}
		
			// last line
        	if ((tm.ParagraphAlignment == TTFText.ParagraphAlignmentEnum.Justified)) {
				ll.align=TTFText.ParagraphAlignmentEnum.Left;
			}
		
			ll.offset=0;
			if (((ll.align != TTFText.ParagraphAlignmentEnum.Right))		
					&& ((ll.align != TTFText.ParagraphAlignmentEnum.Center))) {
					ll.offset=firstlineoffset;		
			}
				
			ll.ComputeCharacterPositions();								
       	 	if ((tm.ParagraphAlignment == TTFText.ParagraphAlignmentEnum.Right)) {
				ll.offset = ll.linewidth-ll.GetActualLinewidth();
			}
        	else {
				if ((tm.ParagraphAlignment == TTFText.ParagraphAlignmentEnum.Center)) {
					ll.offset = (ll.linewidth-ll.GetActualLinewidth())/2;
				}
				else {
					ll.offset=firstlineoffset;								
				}
			}
			ll.lineno=lno++;
				
			yield return ll;
			if ((tm.ParagraphAlignment!=TTFText.ParagraphAlignmentEnum.Right)&&(tm.ParagraphAlignment!=TTFText.ParagraphAlignmentEnum.Center)) {
					firstlineoffset=tm.FirstLineOffset;
			}		
		}
			
		}
	}
	
	
	
	
	public class TTFTextScriptTextToken {
		public bool is_script;
		public string text;
	}
	
	
		
	// styles are defined like this 
	// text <@#ts.Push().SetSlant(0.3f)@> continue <@#ts.Pop()@>
	// text <@#tm.get_style("/template/TTFred")@> truc
	static public IEnumerable<TTFTextScriptTextToken> ParseScriptText(string s) {
		int state=0;
		// 0 : text
		// 1 : 0+@
		// 2 : 0+\
		// 3 : code
		// 4 : 3 + }
		// 5 : 3 + \
		string restext="";
		foreach (char c in s) { 
			switch (state) {
			case 0:
				if (c==open_markup[0]) {
					state=1;
				}
				else {
					if (c=='\\') {
						state=2;
					}
					else {
						restext+=c;
					}
				}
				break;
			case 1:
				if (c==open_markup[1]) {
					state=3;					
					if (restext.Length!=0) {
						TTFTextScriptTextToken ttstt= new TTFTextScriptTextToken();
						ttstt.text=restext;
						ttstt.is_script=false;
						yield return ttstt;
					}
					restext="";	
				}
				else {
					state=0;
					restext+=open_markup[0]+c;
				}
				break;
			case 2:
				state=0;
				restext+=c;
				break;
				
			case 3:
				if (c==close_markup[0]) {
					state=4;
				}
				else {
					if (c=='\\') {
						state=5;
					}
					else {
						restext+=c;
					}
				}
				break;
			case 4:
				if (c==close_markup[1]) {
					state=0;
					if (restext.Length!=0) {
						TTFTextScriptTextToken ttstt= new TTFTextScriptTextToken();
						ttstt.text=restext;
						ttstt.is_script=true;
						yield return ttstt;
					}
					restext="";	
				}
				else {
					state=3;
					restext+=close_markup[0]+c;
				}
				break;
			case 5:
				state=3;
				restext+=c;
				break;
				
			}
		}
			if (state!=0) {
				throw new System.Exception("Parse error");
			}
			if (restext.Length!=0) {
					TTFTextScriptTextToken ttstt= new TTFTextScriptTextToken();
					ttstt.text=restext;
					ttstt.is_script=false;
					yield return ttstt;			
		}
		
	}
	
	public static void EvalScriptText(TTFText tm,string s) {
#if! TTFTEXT_LITE

		//
		TTFTextStyle ts=tm.CurrentTextStyle;
		Jyc.Expr.Parser ep=new Jyc.Expr.Parser();
		Jyc.Expr.Tree tree;
		try {
        	tree = ep.Parse(s);
    		Jyc.Expr.Evaluator evaluater=new Jyc.Expr.Evaluator(); 
    		Jyc.Expr.ParameterVariableHolder pvh= new Jyc.Expr.ParameterVariableHolder() ;
			pvh.Parameters["T"]=pvh.Parameters["text"]=new Jyc.Expr.Parameter(tm);
			pvh.Parameters["S"]=pvh.Parameters["style"]=new Jyc.Expr.Parameter(ts);
		    evaluater.VariableHolder = pvh;
			ts=(TTFTextStyle)evaluater.Eval(tree);
			if (ts!=null) {
			  //Debug.Log (ts.Size);
			  tm.CurrentTextStyle=ts;
			}
		}
		catch (System.Exception e){Debug.LogError(e);}			
#endif				

	}
	
	private const string tpn_float="Single";
	private const string tpn_bool="Bool";
	private const string tpn_int="Int";
	private const string tpn_string="String";
	
	// EASY MARKUP IS AN ALTERNATIVE TO SCRIPT HOWEVER IT IS CURRENTLY MORE AND LESS POWERFUL
	public static void EvalEasyMarkUp(TTFText tm,string s) {
#if TTFTEXT_LITE
			if (tm.DemoMode) {
#endif				

		TTFTextStyle ts=tm.CurrentTextStyle;
		try {
			s=s.Trim();
			if (s[0]=='#') {
				EvalScriptText (tm,s.Substring(1));
			}
			if (s.Contains("=")) {				
				char [] seps={'='};
				string [] vals=s.Split(seps);
				string lval=vals[0].Trim();
			    string rval=vals[1].Trim();
				
				if (lval=="style") {
					if (rval.Contains("<")) {
						// probably invalid
						return;
					}
					ts=ts.PushF(rval);
				}
				else {
					System.Type tp=typeof(TTFTextStyle);						
					System.Reflection.PropertyInfo pi=tp.GetProperty(lval);
					
					string tpn=pi.PropertyType.Name;
						
					ts=ts.Push();
					if (tpn==tpn_float) {							
							if (rval[0]=='+') {
								pi.SetValue(ts,((float)pi.GetValue(ts,null))+System.Single.Parse(rval.Substring(1)),null);
							}
							else {
								if (rval[0]=='*') {
									pi.SetValue(ts,((float)pi.GetValue(ts,null))*System.Single.Parse(rval.Substring(1)),null);
								}
								else {
									pi.SetValue(ts,System.Single.Parse(rval),null);
								}
							}
					}
					else {						
						if (tpn==tpn_bool) {
							pi.SetValue(ts,System.Boolean.Parse(rval),null);
						}
						else {						
							if (tpn==tpn_int) {								
								pi.SetValue(ts,System.Int32.Parse(rval),null);								
							}
							else {
								if (tpn==tpn_string) {
									pi.SetValue(ts,rval,null);								
								}
								else {
									Debug.LogWarning(pi.PropertyType.Name+" : Behaviour not defined");
								}
							}
						}
						
					}
				}
				
			}
			else {
				if ((s.ToLower()=="end")||(s.ToLower()=="pop")) {
					ts=ts.Pop();
				}
			}
			tm.CurrentTextStyle=ts;
		}
		catch (System.Exception e){Debug.LogError(e);}			
#if TTFTEXT_LITE
		}
#endif				

	}
		
	
	public static IEnumerable<LineLayout> GenerateTextLayout(string text, TTFText tm) {				
		switch (tm.LayoutMode) {
            case TTFText.LayoutModeEnum.No:
				char [] seps={'\n'};
				int lineno=0;
				foreach(string s in text.Split(seps)) {
					LineLayout l=new LineLayout(s,tm,true);
					l.offset=0;
					l.lineno=lineno++;
					l.advancedir= Vector3.right;
					yield return l;
				}
				break;
            case TTFText.LayoutModeEnum.Wrap:
#if TTFTEXT_LITE
			if (tm.DemoMode) {
#endif				
			
				foreach (LineLayout ll in WrapParagraphsSimple(text, tm)) {
					yield return ll;
				}
#if TTFTEXT_LITE
			}
#endif				
				
				break;
            case TTFText.LayoutModeEnum.StyleEnabled:
#if TTFTEXT_LITE
			if (tm.DemoMode) {
#endif				
			
				foreach (LineLayout ll in WrapParagraphsExt(text, tm)) {
					yield return ll;
				}
#if TTFTEXT_LITE
			}
#endif				
				
				break;			
		}
	}

#endregion
	
	
#region FUNCTIONS_GENERATING_EXTRUSIONS_AND_MESHES_FOR_THE_WHOLE_TEXT
   // THIS FUNCTION BUILD INDIVIDUAL SUBOBJECTS FOR EACH OBJECT		
   public static int BuildTextViaSubObjects(LineLayout ll,
									        LineSplitter split, 
		                                    int idx, Vector3 pos, TTFText tm, out Bounds bounds) {				
		Vector3 adv = Vector3.zero;

		bounds = new Bounds(Vector3.zero, Vector3.zero);
				
		int num = 0;
		foreach (LineLayout l in split(ll)) {
			//Debug.Log("ll=" + l.line + " pos=" + pos + " charpos0=" + l.charpositions[0] + " advlen=" + l.advance);			
			GameObject go;
			bool b0=((l.line.Length>0)&&(l.GetCharStyle(0).GlyphPrefab!=null));
			if (b0||(tm.GlyphPrefab != null)) {
				if (b0) {
					go = GameObject.Instantiate(l.GetCharStyle(0).GlyphPrefab) as GameObject;
				}
				else {
					go = GameObject.Instantiate(tm.GlyphPrefab) as GameObject;
				}
			} else {
				go = new GameObject();
				go.AddComponent<MeshFilter>();
				go.AddComponent<MeshRenderer>();
				if (tm.gameObject.renderer!=null) {
						
						if (tm.gameObject.renderer.sharedMaterials!=null) {
						    if ((tm.gameObject.renderer.sharedMaterials.Length!=0)&&(tm.gameObject.renderer.sharedMaterials[0]!=null)) {
							  go.renderer.sharedMaterials=tm.gameObject.renderer.sharedMaterials;
						   }
						}
						else {
							if (tm.gameObject.renderer.sharedMaterial!=null) {
								go.renderer.sharedMaterial=tm.gameObject.renderer.sharedMaterial;
							}	
						}
				}
			}
			
			
			int no = idx + num;
			go.name = System.String.Format("{0}. {1}", no, l.line);
			go.transform.parent = tm.transform;

			if (/*tm.rebuildLayout ||*/ (!tm.SaveTokenPos) || tm.TokenPos.Count <= no) {				
				go.transform.localPosition = pos;
				go.transform.localRotation = Quaternion.identity;				
			} else {
				TTFText.TrInfo tr = tm.TokenPos[no];
				go.transform.localPosition = tr.localPosition;
				go.transform.localRotation = tr.localRotation;
				go.transform.localScale = tr.localScale;
			}
				if (l.line.Length>0) {
				if ((l.GetCharStyle(0).sharedMaterials!=null)
					&&(l.GetCharStyle(0).sharedMaterials.Length>0)
					&&(l.GetCharStyle(0).sharedMaterials[0]!=null)) {
						go.renderer.sharedMaterials=l.GetCharStyle(0).sharedMaterials;
				}
				}
			
			
			Mesh mesh = new Mesh();
			TTFTextInternal.BuildMesh(ref mesh, l,  tm, out adv);
			Bounds b = mesh.bounds;
			b.center = b.center + pos;
			MergeBounds(ref bounds, b);				
		    UpdateComponentsWithNewMesh(go, mesh);
			
			pos += l.advance;
#if TTFTEXT_LITE
			if (tm.DemoMode) {
#endif				

			TTFSubtext st=go.GetComponent<TTFSubtext>();
			if (st == null) {
				st = go.AddComponent<TTFSubtext>();
			}
			
#if !TTFTEXT_LITE				
			st.Layout=l;
#endif				
			st.Text=l.line;
			st.LineNo=l.lineno;
			st.SequenceNo=idx+num;
			st.Advance=adv;
#if TTFTEXT_LITE
			}
#endif				

			num++;
		}
		
		return num;
	}


	public static Mesh BuildMesh(ref Mesh mesh, LineLayout ll,  TTFText tm, out Vector3 advance) {
		
		Mesh front = null;
		Mesh back = null;
		TTFTextOutline[] outlines = {};
		bool[] mask;
		advance=Vector3.zero;
		
		TTFTextStyle ts=tm.InitTextStyle;
		
		
		if ((ll.charstyleindex!=null)&&(ll.charstyleindex[0]!=-1)) {
			ts=ll.GetCharStyle(0);
		}
		
		
		
		if (ts.ExtrusionMode == TTFText.ExtrusionModeEnum.None) { // No extrusion		
			TTFTextOutline o = MakeOutline(ll,  ts.Embold, tm);
			o = o.Simplify(ts.SimplifyAmount,ts.Size);
            if (ts.Slant != 0) {
				o.Slant(ts.Slant);
			}
			advance = o.advance;		
			front = TTFTextInternalTesselators.Triangulate(o, tm.DynamicTextRuntimeTriangulationMethod==TTFText.DynamicTextRuntimeTriangulationMethodEnum.CSharpLibs);
			
			if (tm.DynamicTextRuntimeTriangulationMethod==TTFText.DynamicTextRuntimeTriangulationMethodEnum.CSharpLibs) {
			 ReverseTriangles(front);
			}	
		
			if (tm.BackFace) {
				back = new Mesh();
				back.vertices = front.vertices;
				back.triangles = front.triangles;
					
				ReverseTriangles(back);
				
				CombineInstance[] combine = new CombineInstance[2];
				combine[0].mesh = front;
				combine[1].mesh = back;
				
				mesh.CombineMeshes(combine, false, false);
				
				DestroyObj(front);
			    DestroyObj(back);
		
			} else {
				CombineInstance[] combine = new CombineInstance[1];
				combine[0].mesh = front;
				mesh.CombineMeshes(combine, false, false);
				DestroyObj(front);
			}
		
		} 
        else if (ts.ExtrusionMode == TTFText.ExtrusionModeEnum.Pipe) {
#if TTFTEXT_LITE        
		if (tm.DemoMode) {	
#endif			
			TTFTextOutline o = MakeOutline(ll,  ts.Embold,tm);

			o=o.Simplify(ts.SimplifyAmount,ts.Size);
			if (ts.Slant != 0) {
				o.Slant(ts.Slant);
			}
			advance = o.advance;
	
			TTFTextInternalMeshGenerators.Piped(ref mesh ,o, ts.Radius, ts.NumPipeEdges);
#if TTFTEXT_LITE        
			}
#endif			
			
		} 

        else {
		
			switch (ts.ExtrusionMode) {

             case TTFText.ExtrusionModeEnum.Simple:
				outlines = MakeSimpleOutlines(ll, tm,ts);
				break;

             case TTFText.ExtrusionModeEnum.Bent:
#if TTFTEXT_LITE        
		if (tm.DemoMode) {	
#endif							
				outlines = MakeBentOutlines(ll, tm,ts);
#if TTFTEXT_LITE        
			}
#endif			
					
				break;

             case TTFText.ExtrusionModeEnum.Bevel:
#if TTFTEXT_LITE        
		if (tm.DemoMode) {	
#endif								
				outlines = MakeBevelOutlines(ll, tm,ts);
#if TTFTEXT_LITE        
			}
#endif			
					
				break;

             case TTFText.ExtrusionModeEnum.FreeHand:
#if TTFTEXT_LITE        
		if (tm.DemoMode) {	
#endif			
				outlines = MakeFreeHandOutlines(ll, tm,ts);
#if TTFTEXT_LITE        
			}
#endif			
					
				break;
			default:
				Debug.LogError("Unexpected Extrusion Mode:" + tm.ExtrusionMode);
				break;
			}		
	
			if (outlines.Length == 0) {
				mesh.Clear();
				advance = Vector3.zero;
				return mesh;
			}
			
			
			//if (ts.OutlineEmbold >= 0) {
			//	outlines = OutlineOutlines(outlines, ts.OutlineEmbold);
			//}
			
		    mask = outlines[0].SimplifyMask(ts.SimplifyAmount,tm.Size);
		
			// front
			TTFTextOutline o = outlines[0].ApplyMask(mask);
			front = TTFTextInternalTesselators.Triangulate(o,
				tm.DynamicTextRuntimeTriangulationMethod==TTFText.DynamicTextRuntimeTriangulationMethodEnum.CSharpLibs);//tm.EmbedCharset);
				
			advance = o.advance;
			advance.z = 0;
				
			//back
			o = outlines[outlines.Length - 1].ApplyMask(mask);
			back = TTFTextInternalTesselators.Triangulate(o,
				tm.DynamicTextRuntimeTriangulationMethod==TTFText.DynamicTextRuntimeTriangulationMethodEnum.CSharpLibs);//tm.EmbedCharset);
			

				
			if (tm.DynamicTextRuntimeTriangulationMethod==TTFText.DynamicTextRuntimeTriangulationMethodEnum.CSharpLibs) {
				ReverseTriangles(front);
			} else {
				ReverseTriangles(back);
			}
			
			//Debug.Log("num of outlines for" + text + " = " + outlines.Length);
			
			TTFTextInternalMeshGenerators.ExtrudeOutlines(ref mesh, front, back, outlines, mask);
			
			DestroyObj(front);
			DestroyObj(back);

			if (! tm.SplitSides) { // merge submeshes
				MergeSubmeshes(mesh);
			}

		}
	
		ComputeUVs2(mesh, ts.UvType, ts.NormalizeUV, ts.UvScaling);
		mesh.RecalculateNormals();
		ComputeTangents(mesh);
				
		return mesh;
	}
	
#endregion
	
	
	
#region FUNCTIONS_GENERATING_OF_OUTLINES_FOR_DIFFERENT_EXTRUSIONS
		
	public static TTFTextOutline[] OutlineOutlines(TTFTextOutline[] outlines, float str) {
		int len =outlines.Length;
		if (len == 0 || str <= 0) { return outlines; }
		
		TTFTextOutline[] res = new TTFTextOutline[len + 2];
		
		res[0] = outlines[0].Embolden(-str);
		for (int i = 0; i < len; ++i) {
			res[i+1] = outlines[i];//new Outline(outlines[i]);
		}
		
		res[len+1] = outlines[len-1].Embolden(-str);
		return res;
	}
	
	// Simple extrusion
	public static TTFTextOutline[] MakeSimpleOutlines(LineLayout ll, TTFText tm, TTFTextStyle ts) {	
		TTFTextOutline o = MakeOutline(ll,   ts.Embold, tm);
		if (ts.Slant != 0) {
			o.Slant(ts.Slant);
		}
		
		TTFTextOutline[] outlines = new TTFTextOutline[2];
		outlines[0] = o;
		outlines[1] = new TTFTextOutline(o);
		outlines[0].Translate(Vector3.forward * (-ts.ExtrusionDepth) / 2);
		outlines[1].Translate(Vector3.forward * ts.ExtrusionDepth /2);
		
		return outlines;
	}

	// FreeHand Curve Extrusion
	public static TTFTextOutline[] MakeFreeHandOutlines(LineLayout ll, TTFText tm,TTFTextStyle ts) {
		//Vector3 min=Vector3.zero; Vector3 max=Vector3.zero;
		Keyframe[] keys = ts.FreeHandCurve.keys;
		
		int NS = keys.Length;
		
		TTFTextOutline[] outlines = new TTFTextOutline[NS];
		float[] z = new float[NS];

		//min = Vector3.zero;
		//max = Vector3.zero;
		//Vector3 [] szs = new Vector3[NS];
		
		for (int i=0;i<NS;i++) {			
// 			Vector3 min_, max_;
			z[i] = keys[i].time * ts.ExtrusionDepth;
			
			outlines[i] = MakeOutline(ll,  ts.Embold + keys[i].value * ts.BevelForce, tm);			
			outlines[i].Translate(Vector3.forward * z[i]);
			
			if (ts.Slant != 0) {
				outlines[i].Slant(ts.Slant);
			}
			
			//outlines[i].GetMinMax(out min_, out max_);
			//szs[i]=max_-min_;
			//min = Vector3.Min(min, min_);
			//max = Vector3.Max(max, max_);
		}
		//Vector3 sz=max-min;
		//for (int i=0;i<NS;i++) {
		//	outlines[i].Translate((sz-szs[i])/2);
		//}
		return outlines;
	}
	
	static TTFTextOutline[] MakeBentOutlines(LineLayout ll, TTFText tm,TTFTextStyle ts) {			
		//Vector3 min=Vector3.zero; Vector3 max=Vector3.zero;
		int NS = ts.ExtrusionSteps.Length; 
		
		TTFTextOutline[] outlines = new TTFTextOutline[NS];
		//Vector3 [] szs = new Vector3[NS];
		
		for (int i = 0; i < NS; i++) {
			
			//Vector3 min_, max_;
			
		    
		    float embold = ts.Embold + ts.BevelForce * Mathf.Sin(i*Mathf.PI/(NS-1));
		    
		    // wierd error with poly2tri/embold for the backface
		    if (i == NS-1) {
			  embold = ts.Embold;		
		    }
				
		    outlines[i] = MakeOutline(ll,  embold, tm);
		
			float z = (ts.ExtrusionSteps[i] - 0.5f) * ts.ExtrusionDepth;// - tm.exstrusionDepth / 2;
			outlines[i].Translate(Vector3.forward * z);
			
			if (ts.Slant != 0) {
				outlines[i].Slant(ts.Slant);
			}
			
			//outlines[i].GetMinMax(out min_, out max_);
			//szs[i]=max_-min_;
			//min = Vector3.Min(min, min_);
			//max = Vector3.Max(max, max_);
		}
		
		//Vector3 sz=max-min;
		//for (int i=0;i<NS;i++) {
		//	outlines[i].Translate((sz-szs[i])/2);
		//}

		return outlines;
	}

	
	static TTFTextOutline[] MakeBevelOutlines(LineLayout ll, TTFText tm, TTFTextStyle ts) {
		//Vector3 min=Vector3.zero; Vector3 max=Vector3.zero;
		//Vector3 min_; Vector3 max_;
		int NS = tm.NbDiv;
		if (NS < 2) { NS = 2; }
		
		TTFTextOutline[] outlines = new TTFTextOutline[NS*2];
		//Vector3[] szs = new Vector3[NS*2];
		
		float bevelDepth = ts.BevelDepth * ts.ExtrusionDepth / 2;
		
		for (int i = 0; i < NS; ++i) {
			
			float f = ((float) i) / ((float) NS - 1); // [0,1]
			
			float embold = ts.Embold + Mathf.Sin(Mathf.Acos(1-f)) * ts.BevelForce;
			
			TTFTextOutline o = MakeOutline(ll,embold,tm);
			
			o.Slant(ts.Slant);
			
			outlines[i] = o;
			outlines[2 * NS - 1 - i] = new TTFTextOutline(o);
			
			//outlines[i].GetMinMax(out min_, out max_);
			//szs[i]=max_-min_;
			float z = f * bevelDepth;
			
			outlines[i].Translate(Vector3.forward * z);
			outlines[2 * NS - 1 - i].Translate(Vector3.forward * (ts.ExtrusionDepth - z));
			
			//szs[2*NS-1-i]=max_-min_;
			//min = Vector3.Min(min, min_);
			//max = Vector3.Max(max, max_);			
		}


		//Vector3 sz=max-min;
		//for (int i=0;i<(2*NS);i++) {
		//	outlines[i].Translate((sz-szs[i])/2);
		//}
		
	
		return outlines;
	}
    
#endregion
	
        public static TTFTextOutline MakeNativeOutline(string txt,						
						float charSpacing, 
						float embold, 
						TTF.Font font,					
						bool reversed, 
		                int interpolationstep
					) {
				TTFTextOutline o=null;
#if !TTFTEXT_LITE
#if UNITY_STANDALONE_WIN || UNITY_STANDALONE_OSX || UNITY_EDITOR

				TTF.Outline.Point tadv = new TTF.Outline.Point();
				TTF.Outline ttfoutline = font.GetStringOutline(
					txt, ref tadv,
					TTF.HorizontalJustification.Origin,
					TTF.VerticalJustification.Origin ,
					false,  
					charSpacing,
			        0,
			        interpolationstep);

		o= TTFTextOutline.TTF2Outline(ttfoutline, tadv,reversed).Embolden(embold);
#endif
#endif				

			return o;
	}
				
	    
	
        public static TTFTextOutline MakeOutline(string txt,						
						float charSpacing, 
						float embold, 
						TTFText tm,					
						bool reversed, 
						float [] charpositions,
						int [] charstyleidx) {

		
		
		
	             TTFTextOutline outline = new TTFTextOutline();
				int fp=0; string currentfontid="";
				object cfont=null;
				object parameters=null;
				TTFTextStyle cttfstyle=null;
		
				if (charstyleidx==null) {
						cttfstyle=tm.InitTextStyle;
						cfont=cttfstyle.GetFont(ref fp, ref currentfontid, ref parameters);
			
						if (cfont==null) {
							throw new System.Exception("Font not found :"+tm.InitTextStyle.FontId);
						}

				}
				if (charpositions != null && charpositions.Length < txt.Length) {
					Debug.LogError("Bad char position len=" + charpositions.Length + " txt = " + txt + " (len=" + txt.Length + ")");
					charpositions = null;
				}
				
		
		
		
		
		
		
				int i=0;
                foreach (char c in txt) {
				    TTFTextOutline o = null;
					if (charstyleidx!=null) {
						if ((cfont==null)||(currentfontid!=tm.UsedStyles[charstyleidx[i]].GetFontProviderFontId(fp))) {
						cttfstyle=tm.UsedStyles[charstyleidx[i]];			
						cfont=cttfstyle.GetFont(ref fp, ref currentfontid, ref parameters);
			
						if (cfont==null) {
							throw new System.Exception("Font not found :"+tm.InitTextStyle.FontId);
						}

						}
					}
        			
			
					o = TTFTextFontProvider.font_providers[fp].GetGlyphOutline(parameters,cfont,c);
                    
					if (charstyleidx!=null) {
					
						for (int ii=0; ii<tm.UsedStyles[charstyleidx[i]].GetOutlineEffectStackElementLength();ii++) {
							TTFTextStyle.TTFTextOutlineEffectStackElement tse=tm.InitTextStyle.GetOutlineEffectStackElement(ii);
							o=TTFTextOutline.AvailableOutlineEffects[tse.id].Apply(o,tse.parameters);
						}
					}
				    else {
						for (int ii=0; ii<tm.InitTextStyle.GetOutlineEffectStackElementLength();ii++) {
							TTFTextStyle.TTFTextOutlineEffectStackElement tse=tm.InitTextStyle.GetOutlineEffectStackElement(ii);
							o=TTFTextOutline.AvailableOutlineEffects[tse.id].Apply(o,tse.parameters);
						}
					}
				
				
				    o = o.Embolden(((charstyleidx!=null)?tm.UsedStyles[charstyleidx[i]].Embold:tm.Embold)+embold);
                    o.Rescale((charstyleidx!=null)?tm.UsedStyles[charstyleidx[i]].Size:tm.Size);
				

					if (charpositions==null) { outline.Append(o, outline.advance); }
					else { outline.Append(o, Vector3.right*charpositions[i]); }
			 		i+=1;
			
                }
                return outline;
		
/*
#if !TTFTEXT_LITE
		
            if ((tm!= null) 
			 && (tm.DynamicTextRuntimeFontProviderMethod==TTFText.DynamicTextRuntimeFontProviderMethodEnum.EmbeddedAndNetworkFonts)
				) {
			
                TTFTextOutline outline = new TTFTextOutline();
				TTFTextFontStore tfs=GameObject.Find("/TTFText Font Store").GetComponent<TTFTextFontStore>();				
				
				TTFTextFontStoreFont tfsf=null;
				if (charstyleidx==null) {
					tfsf=tfs.GetFont(tm.FontId);
				}
				if (charpositions != null && charpositions.Length < txt.Length) {
					Debug.LogError("bad char position len=" + charpositions.Length + " txt = " + txt + " (len=" + txt.Length + ")");
					charpositions = null;
				}
				
				int i=0;
                foreach (char c in txt) {
				    TTFTextOutline o = null;
					if (charstyleidx!=null) {
						if ((tfsf==null)||(tfsf.fontid!=tm.UsedStyles[charstyleidx[i]].FontId)) {
							tfsf=tfs.GetFont(tm.UsedStyles[charstyleidx[i]].FontId);
						}
					}
                    if (c >= 0x20 && c < 0x7f) { // Ascii charset
					    o = tfsf.charset[(byte)c];
                    } else { // try custom char list
                        int idx = tfsf.additionalChar.IndexOf(c);
                        if (idx >= 0 && idx < tfsf.addCharset.Length) {
							o = tfsf.addCharset[idx];                        
					    } else { // fallback on space char
                            Debug.LogWarning("Character '" + c + "' not found. Consider adding it in additonnal char text field.");
							o = tfsf.charset[(byte) ' '];
                        }
                    }


					if (charstyleidx!=null) {
					
						for (int ii=0; ii<tm.UsedStyles[charstyleidx[i]].GetOutlineEffectStackElementLength();ii++) {
							TTFTextStyle.TTFTextOutlineEffectStackElement tse=tm.InitTextStyle.GetOutlineEffectStackElement(ii);
							o=TTFTextOutline.AvailableOutlineEffects[tse.id].Apply(o,tse.parameters);
						}
					}
				    else {
						for (int ii=0; ii<tm.InitTextStyle.GetOutlineEffectStackElementLength();ii++) {
							TTFTextStyle.TTFTextOutlineEffectStackElement tse=tm.InitTextStyle.GetOutlineEffectStackElement(ii);
							o=TTFTextOutline.AvailableOutlineEffects[tse.id].Apply(o,tse.parameters);
						}
					}
				
				
				    o = o.Embolden(((charstyleidx!=null)?tm.UsedStyles[charstyleidx[i]].Embold:tm.Embold)+embold);
                    o.Rescale((charstyleidx!=null)?tm.UsedStyles[charstyleidx[i]].Size:tm.Size);
				
				
				
					if (charpositions==null) {
                    	outline.Append(o, outline.advance);
					}
					else {
						outline.Append(o, Vector3.right*charpositions[i]);
						i+=1;
					}
                }
                return outline;
		
		} else {
#endif						
#if UNITY_STANDALONE_WIN || UNITY_STANDALONE_OSX || UNITY_EDITOR
			TTFTextOutline o=null;
			if (charpositions==null) {
				TTF.Outline.Point tadv = new TTF.Outline.Point();
				TTF.Font font=TryOpenFont(tm.InitTextStyle.FontId,tm.InitTextStyle.OrientationReversed,tm.InitTextStyle.runtimeFontFallback);
				if (font==null) {
							new System.Exception("Font not found :"+tm.InitTextStyle.FontId);
				}

				
				
				TTF.Outline ttfoutline = font.GetStringOutline(
					txt, ref tadv,
					TTF.HorizontalJustification.Origin, TTF.VerticalJustification.Origin ,
					false,  charSpacing, 0, ((tm!=null) ?( tm.InterpolationSteps):4));
				o= TTFTextOutline.TTF2Outline(ttfoutline, tadv,reversed).Embolden(embold);
				if ((tm !=null )&&(tm.Size!=1)) { o.Rescale(tm.Size); } 
				font.Dispose();
			}
			else {
				o=new TTFTextOutline();
				TTF.Outline.Point tadv = new TTF.Outline.Point();
				TTF.Font font=null;
				if (charstyleidx==null) {
						font=TryOpenFont(tm.InitTextStyle.FontId,tm.InitTextStyle.OrientationReversed,tm.InitTextStyle.runtimeFontFallback);
						if (font==null) {
							new System.Exception("Font not found :"+tm.InitTextStyle.FontId);
						}
					
				}

				for (int i=0;i<txt.Length;i++) {
					
					if (charstyleidx!=null) {
						//Debug.Log(System.String.Format("{0} - {1} - {2}",i,txt[i], GetCharStyle(i).FontId));						
						if ((font==null)||(tm.UsedStyles[charstyleidx[i]].FontId!= font.Name)) {
							if (font!=null) {
								font.Dispose(); font=null;
							}
							font=TryOpenFont(tm.UsedStyles[charstyleidx[i]].FontId,tm.UsedStyles[charstyleidx[i]].OrientationReversed,tm.UsedStyles[charstyleidx[i]].runtimeFontFallback);
							if (font==null) {
								new System.Exception("Font not found :"+tm.UsedStyles[charstyleidx[i]].FontId);
							}
							
						}
					}	
					
					if (font==null) {
						Debug.LogWarning("font is null / this shall not happen" + tm.FontId);
					}
					TTF.Outline to=font.GetGlyphOutline( txt[i], out tadv, true, 0,tm.InterpolationSteps);
					TTFTextOutline o2=TTFTextOutline.TTF2Outline(to,tadv,reversed);
					
					
					if (charstyleidx!=null) {
						for (int ii=0; ii<tm.UsedStyles[charstyleidx[i]].GetOutlineEffectStackElementLength();ii++) {
							TTFTextStyle.TTFTextOutlineEffectStackElement tse=tm.InitTextStyle.GetOutlineEffectStackElement(ii);
							o2=TTFTextOutline.AvailableOutlineEffects[tse.id].Apply(o2,tse.parameters);
						}
					}
				    else {
						for (int ii=0; ii<tm.InitTextStyle.GetOutlineEffectStackElementLength();ii++) {
							TTFTextStyle.TTFTextOutlineEffectStackElement tse=tm.InitTextStyle.GetOutlineEffectStackElement(ii);
							o2=TTFTextOutline.AvailableOutlineEffects[tse.id].Apply(o2,tse.parameters);
						}
					}
					

					
					if (charstyleidx!=null) {
						o2=o2.Embolden(tm.UsedStyles[charstyleidx[i]].Embold+embold);
						if (tm.UsedStyles[charstyleidx[i]].Size!=1) {o2.Rescale(tm.UsedStyles[charstyleidx[i]].Size);}						
					}
					else {
						o2=o2.Embolden(embold);
						if ((tm !=null )&&(tm.Size!=1)) {o2.Rescale(tm.Size);}
					}
					
					
					
					o.Append(o2, Vector3.right*charpositions[i]);
				}	
				if (font!=null) {
					font.Dispose();
				}
			}
				
			return o;	
#else
			Debug.LogError("Native fontmesh rendering not yet supported on this platform");
			return null;
#endif
#if !TTFTEXT_LITE			
		}
#endif
*/					
	
	}

    public static TTFTextOutline MakeOutline(string txt, float charSpacing, float embold, TTFText tm) {
			return MakeOutline(txt,charSpacing,embold,tm,tm.OrientationReversed,null,null);
	}

    public static TTFTextOutline MakeOutline(LineLayout ll,  float embold, TTFText tm) {
			return MakeOutline(ll.line,ll.hspacing,embold,tm,tm.OrientationReversed,ll.charpositions,ll.charstyleindex);
	}
		
#region FUNCTIONS_RELATED_TO_SHADERMAPS

    static void ComputeUVs2(Mesh mesh, TTFText.UVTypeEnum uvType, bool normalized, Vector3 uvscale)
    {
	
		Vector3 e = mesh.bounds.extents;
		Vector3 c = mesh.bounds.center;
	
		Vector3 invse = new Vector3((e.x!=0)?(1f/e.x):1f,(e.y!=0)?(1f/e.y):1f,(e.z!=0)?(1f/e.z):1f);
		Vector3[] vertices = mesh.vertices;
		
		if (vertices.Length == 0) { return; }
		
		Vector2[] uvs = new Vector2[vertices.Length];
		
		Vector3 a;		
		for (int i = 0; i < vertices.Length; ++i) {
			
			if (uvType == TTFText.UVTypeEnum.Box) {
				a = vertices[i];
				if (normalized) {
					a = Vector3.Scale(vertices[i],invse);
				}
			} else { // Spherical
				a = Quaternion.FromToRotation(Vector3.forward,vertices[i]-c).eulerAngles;
				a /= 360;//(Mathf.PI*2);
			}
			
			uvs[i].x = (uvscale.x * a.x + uvscale.z * a.z);
			uvs[i].y = (uvscale.y * a.y + uvscale.z * a.z);
		}
		
		mesh.uv = uvs;
	}

	

	// Compute Normals for the given mesh
	// currently overrides computed value with (0,0,-1) for front side and (0, 0, 1) for back side
	// perhaps this is not a good idea for contour vertices?
	private static void RecalculateNormals(Mesh mesh, int frontCap, int backCap, int size) {
		mesh.RecalculateNormals();
		Vector3[] normals = mesh.normals;
		for (int idx = frontCap; idx < frontCap + size; ++idx) {
			normals[idx] = - Vector3.forward;
		}
		for (int idx = backCap; idx < backCap + size; ++ idx) {
			normals[idx] = Vector3.forward;
		}
		mesh.normals = normals;
	}
	
	
	
	// Compute Mesh Tangents
	// uv and normals array should already be assigned
	// Taken from http://answers.unity3d.com/questions/7789/calculating-tangents-vector4.html
	// which is derived from
	// Lengyel, Eric. 
	//	“Computing Tangent Space Basis Vectors for an Arbitrary Mesh”. Terathon Software 3D Graphics Library, 2001.
	// [url]http://www.terathon.com/code/tangent.html[/url]
	
	public static void ComputeTangents(Mesh mesh) {

		//speed up math by copying the mesh arrays
	    int[] triangles = mesh.triangles;
	    Vector3[] vertices = mesh.vertices;
	    Vector2[] uv = mesh.uv;
	    Vector3[] normals = mesh.normals;
	
	    //variable definitions
	    int triangleCount = triangles.Length;
	    int vertexCount = vertices.Length;
	
	    Vector3[] tan1 = new Vector3[vertexCount];
	    Vector3[] tan2 = new Vector3[vertexCount];
	
	    Vector4[] tangents = new Vector4[vertexCount];
	
	    for (int a = 0; a < triangleCount; a += 3)
	    {
	        int i1 = triangles[a + 0];
	        int i2 = triangles[a + 1];
	        int i3 = triangles[a + 2];
	
	        Vector3 v1 = vertices[i1];
	        Vector3 v2 = vertices[i2];
	        Vector3 v3 = vertices[i3];
	
	        Vector2 w1 = uv[i1];
	        Vector2 w2 = uv[i2];
	        Vector2 w3 = uv[i3];
	
	        float x1 = v2.x - v1.x;
	        float x2 = v3.x - v1.x;
	        float y1 = v2.y - v1.y;
	        float y2 = v3.y - v1.y;
	        float z1 = v2.z - v1.z;
	        float z2 = v3.z - v1.z;
	
	        float s1 = w2.x - w1.x;
	        float s2 = w3.x - w1.x;
	        float t1 = w2.y - w1.y;
	        float t2 = w3.y - w1.y;
	
	        float r = 1.0f / (s1 * t2 - s2 * t1);
	
	        Vector3 sdir = new Vector3((t2 * x1 - t1 * x2) * r, (t2 * y1 - t1 * y2) * r, (t2 * z1 - t1 * z2) * r);
	        Vector3 tdir = new Vector3((s1 * x2 - s2 * x1) * r, (s1 * y2 - s2 * y1) * r, (s1 * z2 - s2 * z1) * r);
	
	        tan1[i1] += sdir;
	        tan1[i2] += sdir;
	        tan1[i3] += sdir;
	
	        tan2[i1] += tdir;
	        tan2[i2] += tdir;
	        tan2[i3] += tdir;
	    }
	
	
	    for (int a = 0; a < vertexCount; ++a) {
			
	        Vector3 n = normals[a];
	        Vector3 t = tan1[a];
	
	        //Vector3 tmp = (t - n * Vector3.Dot(n, t)).normalized;
	        //tangents[a] = new Vector4(tmp.x, tmp.y, tmp.z);
	        Vector3.OrthoNormalize(ref n, ref t);
	        tangents[a].x = t.x;
	        tangents[a].y = t.y;
	        tangents[a].z = t.z;
	
	        tangents[a].w = (Vector3.Dot(Vector3.Cross(n, t), tan2[a]) < 0.0f) ? -1.0f : 1.0f;
	    }
	
	    mesh.tangents = tangents;
	}
#endregion

		
#region UTILITY_FUNCTIONS
	
	// Utility functions

	
	public static void UpdateComponentsWithNewMesh(GameObject go, Mesh mesh) {
		// Update MeshFilter and/or InteractiveCloth components if present
		
		MeshFilter mf = go.GetComponent<MeshFilter>();
		if (mf != null) {
			mf.sharedMesh = mesh;
		}
		
#if !UNITY_FLASH		
		InteractiveCloth ic = go.GetComponent<InteractiveCloth>();
		if (ic != null) {
			ic.mesh = mesh;
		}
#endif		
		
		// Update Box/Mesh Collider Bounds if present
		if (go.collider is BoxCollider) {
			BoxCollider bc = go.collider as BoxCollider;
			Bounds b = mesh.bounds;
			bc.center = b.center;
			bc.size = b.size;
		} else if (go.collider is MeshCollider) {
			MeshCollider mc = go.collider as MeshCollider;
			mc.sharedMesh = mesh;
		}
		
#if TTFTEXT_LITE       
		TTFText tm=go.GetComponent<TTFText>();
		if (tm==null) {
			tm=go.transform.parent.GetComponent<TTFText>();
		}
		if (tm.DemoMode) {
#endif			

		// Release the Mesh on GameObject destruction
		ReleaseMeshOnDestroy rm = go.GetComponent<ReleaseMeshOnDestroy>();
		if (rm == null) {
			rm = go.AddComponent<ReleaseMeshOnDestroy>();
		}
		rm.mesh = mesh;
		
		go.SendMessage("MeshUpdated", SendMessageOptions.DontRequireReceiver);
#if TTFTEXT_LITE        
			}
#endif			

		
	}
	
	
	static void ReverseTriangles(Mesh m) {
	
		int[] t = m.triangles;
		
		// reverse triangles orientation 
		
        for (int i = 0; i < t.Length; i += 3) {
			int k = t[i+1];
			t[i+1]= t[i+2];
			t[i+2]= k;
		}
		
		m.triangles = t;
	}
	
	
	static void TranslateMesh(Mesh m, Vector3 tr) {
		Vector3[] vertices = m.vertices;
		for (int i = 0; i < m.vertexCount; ++i) {
			vertices[i] += tr;
		}
		m.vertices = vertices;
	}
	
	// Merge Submeshes into one
	private static void MergeSubmeshes(Mesh mesh) {
		
		List<int> triangles = new List<int>();
		
		for (int i = 0; i < mesh.subMeshCount; ++i) {
			triangles.AddRange(mesh.GetTriangles(i));
		}
			
		mesh.triangles = triangles.ToArray();
	}

	
	/*
	static int BuildLineLayout(LineLayout ll,  int idx, Vector3 pos, TTFText tm, out Bounds bounds) {
		return BuildTextViaSubObjects(ll,lineSplitters[(int)tm.TokenMode],idx,pos,tm, out bounds);
	}
	*/

	static void MergeBounds(ref Bounds b, Bounds other) {
		if (other.size == Vector3.zero) { return; }
		if (b.size == Vector3.zero) { b = other; return; }
		b.Encapsulate(other);
	}
	
	
	public static void ResetChildren(TTFText tm) {
		
		List<Transform> l = new List<Transform>();
		
		
		// keep track of token previous positions
		if (tm.SaveTokenPos) {tm.TokenPos.Clear();}
		foreach(Transform t in tm.transform) {
			if (tm.SaveTokenPos) {
				tm.TokenPos.Add(new TTFText.TrInfo(t));
			}
			l.Add(t);
			
		}
		
		foreach(Transform t in l) {
			DestroyObj(t.gameObject);
		}
	}
	
	static void ResetTextMesh(TTFText tm) {
		
		ResetChildren(tm);
		
		MeshFilter mf = tm.GetComponent<MeshFilter>();
		if (mf != null) {
			mf.sharedMesh = null;
		}
		
		if (tm.mesh != null) {
			DestroyObj(tm.mesh);
			tm.mesh = null;
		}
		
		tm.advance = Vector3.zero;
	}

	
	static public void DestroyObj(Object o) {
		if ((Application.isEditor)&&(!(Application.isPlaying))) {
			GameObject.DestroyImmediate(o);
		} else  {
			GameObject.DestroyObject(o);
		}
	}
	
	static int ExpectedNumberOfSubmeshes(TTFText tm) {	
			int nsubmeshes=1;
			if ((tm.SplitSides)) {	
				nsubmeshes=3;
				if (tm.ExtrusionMode==TTFText.ExtrusionModeEnum.Pipe) {
					nsubmeshes=1;
				}
				if (tm.ExtrusionMode==TTFText.ExtrusionModeEnum.None) {
					if (! tm.BackFace)
						nsubmeshes=1;
					else 
						nsubmeshes=2;
				}
			}
		return nsubmeshes;
	}


		
		
	// THESE FUNCTION MAY BE USE TO CALCULATE AN APPROXIMATIVE BOUNDING BOX FROM FANCY OUTLINES
     public static Vector2 StdDiff(Vector2 [] a) {
		Vector2 m=Vector2.zero;
		Vector2 e=Vector2.zero;
		Vector2 f=Vector2.zero;
		if (a.Length==0) return Vector3.zero;	
		Vector2 pv=a[a.Length-1];
		foreach(Vector2 nv in a) {
			m+=nv;
			f.x+=(nv.y-pv.y);
			f.y+=(nv.x-pv.x);
			e.x+=  (nv.x+pv.x)*(nv.x+pv.x)*f.x;
			e.y+=  (nv.y+pv.y)*(nv.y+pv.y)*f.y;
			pv=nv;
		}
		m/=a.Length;
		e=new Vector2(e.x/(4*f.x), e.y/(4*f.y));
		return new Vector2(Mathf.Sqrt(e.x-m.x*m.x),Mathf.Sqrt(e.y-m.y*m.y) );
	}

		
     public static Vector3 StdDiff(Vector3 [] a) {
		Vector3 m=Vector3.zero;
		Vector3 e=Vector3.zero;
		Vector3 f=Vector3.zero;
		Vector3 g=Vector3.zero;
		Vector3 sv=Vector3.zero;
		
		if (a.Length==0) return Vector3.zero;	
			
			
		Vector3 pv=a[a.Length-1];
			
		foreach(Vector3 nv in a) {
			//m+=nv;
			sv=nv+pv;
			f.x=Mathf.Abs(nv.y-pv.y);
			f.y=Mathf.Abs(nv.x-pv.x);
			g+=f;
			e.x+=sv.x*sv.x*f.x;
			e.y+=sv.y*sv.y*f.y;
			m.x+=sv.x*f.x;
			m.y+=sv.y*f.y;					
			pv=nv;
		}
			
		m=new Vector3(m.x/2*g.x,m.y/2*g.y,0);
		e=new Vector3(e.x/(4*g.x), e.y/(4*g.y),0);
		return new Vector3(Mathf.Sqrt(Mathf.Abs(e.x-m.x*m.x)),Mathf.Sqrt(Mathf.Abs(e.y-m.y*m.y)),0 );
	}
		
		
     public static Vector2 StdXDiff(Vector2 [] a,float deg) {
		Vector2 m=Vector2.zero;
		Vector2 e=Vector2.zero;
		Vector2 f=Vector2.zero;
		Vector2 g=Vector2.zero;
		if (a.Length==0) return Vector2.zero;	
		Vector2 pv=a[a.Length-1];
		foreach(Vector2 nv in a) {
			m+=nv;
			f.x=Mathf.Abs(nv.y-pv.y);
			f.y=Mathf.Abs(nv.x-pv.x);
			g+=f;
			e.x+=  Mathf.Pow(Mathf.Abs(nv.x+pv.x),deg)*f.x;
			e.y+=  Mathf.Pow(Mathf.Abs(nv.y+pv.y),deg)*f.y;
			pv=nv;
		}
		m/=a.Length;
		e=new Vector2(e.x/(Mathf.Pow(2,deg)*g.x), e.y/(Mathf.Pow(2,deg)*g.y));
		return new Vector2(Mathf.Pow(e.x-Mathf.Pow(m.x,deg),1.0f/deg),Mathf.Pow(e.y-Mathf.Pow(m.x,deg),1.0f/deg) );
	}
	
		
		
		
		
		
    // Try Open Font specified by fontId
    // 1. if fontID = "", set it to a sane value first
    // 2. try open it
    // 3. if fail, try fallback fonts
    // 5. if fail return null
    // When no more in use, fhe font should be disposed using font.Dispose() method

	
	   static public TTF.Font TryOpenFont(string fontId,bool reversed, string runtimeFontFallback)
    {

#if UNITY_STANDALONE_WIN || UNITY_STANDALONE_OSX || UNITY_EDITOR
        TTFTextFontListManager flm = TTFTextFontListManager.Instance;
        TTF.Font font = null;

        if (flm.Count == 0) { return null; }

        // 1. set fontId to a sane value
        // do not override fontId if it's already set
        if (fontId == "")
        {
            fontId = flm.GetOneId();
        }

        // Try Open it

        font = flm.OpenFont(fontId, 1, ref reversed);

        if (font != null) { return font; }

        Debug.LogWarning("Font '" + fontId + "' not found.");

#if !TTFTEXT_LITE
        char[] sep = new char[] { ';' };
		
        foreach (string s in runtimeFontFallback.Split(sep, System.StringSplitOptions.RemoveEmptyEntries))
        {

            font = flm.OpenFont(s, 1, ref reversed);

            if (font != null)
            {
                Debug.Log("Found fallback font " + font.Name);
                return font;
            }
        }
#endif
        // Last resort try another font

        font = flm.OpenFont(flm.GetOneId(), 1, ref reversed);

        if (font != null)
        {
            Debug.Log("Found fallback font " + font.Name);
            return font;
        }
		
		

#endif
        return null;
    }
	
	
    static public TTF.Font TryOpenFont(TTFTextStyle ts,float size)
    {

#if UNITY_STANDALONE_WIN || UNITY_STANDALONE_OSX || UNITY_EDITOR

        TTFTextFontListManager flm = TTFTextFontListManager.Instance;
        TTF.Font font = null;

        if (flm.Count == 0) { return null; }

        // 1. set fontId to a sane value
        // do not override fontId if it's already set
        if (ts.FontId == "")
        {
            ts.FontId = flm.GetOneId();
        }

        // Try Open it

        font = flm.OpenFont(ts.FontId, size, ref ts.orientationReversed);

        if (font != null) { return font; }

        Debug.LogWarning("Font '" + ts.FontId + "' not found.");


        // Try fallback fonts
#if !TTFTEXT_LITE
        char[] sep = new char[] { ';' };
		
        foreach (string s in ts.runtimeFontFallback.Split(sep, System.StringSplitOptions.RemoveEmptyEntries))
        {

            font = flm.OpenFont(s, size, ref ts.orientationReversed);

            if (font != null)
            {
                Debug.Log("Found fallback font " + font.Name);
                return font;
            }
        }
#endif
        // Last resort try another font

        font = flm.OpenFont(flm.GetOneId(), size, ref ts.orientationReversed);

        if (font != null)
        {
            Debug.Log("Found fallback font " + font.Name);
            return font;
        }

#endif
        ts.orientationReversed = false;
        return null;
    }
	
	public static TTF.Font TryOpenFont(TTFTextStyle ts) {
		return TryOpenFont(ts, ts.Size);
	}	
	
#endregion

}
