using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TTFTextInternalAndroid {
#if UNITY_ANDROID  
	
  private static  TTFTextInternalAndroid _Instance = null;
	
  private AndroidJavaClass AndroidCSysFont;	
//  private AndroidJavaClass AndroidCTypeFace;
 // private AndroidJavaClass AndroidCPath;	
//  private AndroidJavaClass AndroidCPathMeasure;
  private AndroidJavaObject AndroidISysFont=null;


 
  public TTFTextInternalAndroid()  {
		AndroidCSysFont=new AndroidJavaClass("com.computerdreams.ttftext.androidsysfont.AndroidSysFont");
		//AndroidCTypeFace = new AndroidJavaClass("android.graphics.TypeFace");				
		//AndroidCPath = new AndroidJavaClass("android.graphics.Path");
		//AndroidCPathMeasure = new AndroidJavaClass("android.graphics.PathMeasure");	
		
		if (AndroidCSysFont==null) {
			Debug.Log("Failed to get the class AndroidSysFont");
		}
		
		
		//AndroidISysFont = AndroidCSysFont.CallStatic<AndroidJavaObject>("GetInstance");
		if (AndroidISysFont==null) {
			AndroidISysFont=new AndroidJavaObject("com.computerdreams.ttftext.androidsysfont.AndroidSysFont");			
		}
		
		if (AndroidISysFont==null) {
			
			Debug.Log("Failed to create the AndroidSysFont Instance");
		}
  }
	
  public static  TTFTextInternalAndroid Instance
  {
    get
    {
      if (_Instance == null)
      {		
		
		_Instance=new TTFTextInternalAndroid();
      }
      return _Instance;
    }
 
  }
	
  public class Font {
		public AndroidJavaObject typeface;
		public Font(AndroidJavaObject jo) {typeface=jo;}
  }
	
  public Font GetTypeface(string text, bool bold, bool italic) {
		return new Font(AndroidISysFont.Call<AndroidJavaObject>("GetTypeface",bold,italic));
  }
	
	
  public float GetGlyphAdvance(Font f, char c) {
		 return AndroidISysFont.Call<float>("GetGlyphWidth", f.typeface, c);
  }
	
  public TTFTextOutline GetGlyph(Font f, char c) {
		AndroidJavaObject pm=
			AndroidISysFont.Call<AndroidJavaObject>("GetPathMeasure",
				AndroidISysFont.Call<AndroidJavaObject>("GetPath",
					f.typeface,
				    c
				)
			);
		
		TTFTextOutline tto=new TTFTextOutline();
		bool cont=true ; 
		while (cont) {
			List<Vector3> tb=new List<Vector3>();

			float [] res=AndroidISysFont.Call<float []>("GetBoundary",pm);
			int rl2=res.Length/2;
			for (int ci=0;ci<rl2;ci++) {
				tb.Add(new Vector3(res[ci*2],res[ci*2+1],0));
			}
			tto.AddBoundary(tb);
			cont=AndroidISysFont.Call<bool>("NextBoundary",pm);
		}
		return tto;
  }
	
  public List<string> EnumFonts() {
		try {
			return new List<string>(AndroidISysFont.Call<string[]>("EnumFonts"));
		}
		catch (System.Exception e){
			Debug.LogError("Error while fetching font list"+e);
			return new List<string>();
		}
  }
 
#endif
}
