//  Unity TTF Text
//  Copyrights 2011-2012 ComputerDreams.org O. Blanc & B. Nouvel
//  All infos related to this software at http://ttftext.computerdreams.org/
//   

using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
[AddComponentMenu("Text/Internal/Release Mesh On Destroy")]
public class ReleaseMeshOnDestroy : MonoBehaviour {
	public Mesh mesh;
		
	void OnDestroy() {
		if (mesh!=null ) {
				
			if ((Application.isEditor)&&(!Application.isPlaying)) {
				Object.DestroyImmediate(mesh);
			}
			else {
			   Object.Destroy(mesh);
			}
		 }
	}
	
}
