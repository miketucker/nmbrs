class ColorSchemes
{

	public static var tommiAr:Array = [Color(99.0/255,147.0/255,63.0/255,1), Color(216.0/255,61.0/255,107.0/255,1), Color(135.0/255,42.0/255,122.0/255,1), Color(84.0/255,156.0/255,215.0/255,1), Color(80.0/255,80.0/255,84.0/255,1)  ];
	public static var origAr:Array = [Color(0.5,1.0,0.5,1.0),Color(1.0,0.0,.5,1.0),Color(0.0,1.0,0.5,1.0),Color(1.0,0.0,1.0,1.0)];


	public static function GetTommi():Color{
		return tommiAr[Random.Range(0,tommiAr.length)];
	}
	public static function GetMike():Color{
		return origAr[Random.Range(0,origAr.length)];
	}

	public static function GetGrey():Color{
		var v = Random.Range(0.1,1.0);
		return Color(v,v,v);
	}

	public static function GetRedish():Color{
		return Color(Random.Range(0.7,1.0), 0 , Random.Range(0.0,1.0));
	}

	public static function GetRandom():Color{
		return Color(Random.Range(0.0,1.0), Random.Range(0.0,1.0) , Random.Range(0.0,1.0));
	}

	public static function GetByMode(n:int){
		switch(n){
			case 1: // LORI D
				return GetGrey();
				break;
			case 2:	// REDINHO
				return GetRedish();
				break;
			case 3: // COLOR
				return GetRandom();
				break;
			case 4: // tommi
				return GetMike();
				break;
			case 5:	// white
				return Color.white;
				break;
			default:
				return GetTommi();
				break;	
		}	


	}
}
