using UnityEngine;
using System.Collections;

public class RedinhoBaller : MonoBehaviour {
	public GameObject prefab;
	public float height = 10.0f;
	public float rad = 5.0f;
	public float freq = 1.0f;
	public GameObject[] letters;
	void Start()
	{
		MakeObject();
		MakeObject();
		MakeObject();
		StartCoroutine(Loop());
	}

	IEnumerator Loop()
	{
		int i = 0;
		while(true)
		{
			MakeObject();
			yield return new WaitForSeconds(freq);
		}
	}

	void MakeObject()
	{
		GameObject g;
		if(Random.value < .5f)
		{
			g = GameObject.Instantiate(prefab) as GameObject;	
		} else {
			g = GameObject.Instantiate(letters[Random.Range(0,letters.Length)]) as GameObject;	
			g.transform.localEulerAngles = Random.insideUnitCircle * 360.0f;
		}
		
		Vector3 v = Vector3.up * height;
		Vector2 r = Random.insideUnitCircle * rad;
		v.x = r.x;
		v.z = r.y;
		g.transform.position = v;
		g.transform.localScale *= Random.Range(1.0f,3.0f);
	}
}
