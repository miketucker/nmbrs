using UnityEngine;
using System.Collections;

public class AutoKill : MonoBehaviour {

	public float lifetime = 30.0f;
	private float life = 0;
	// Use this for initialization
	void Start () {
		life = Time.time;
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		if(Time.time - life > lifetime) Destroy(gameObject);
	}
}
