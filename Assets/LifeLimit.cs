using UnityEngine;
using System.Collections;

public class LifeLimit : MonoBehaviour {

	private float t = 0.0f;
	private float life = 10.0f;
	private Material mat;
	private float fadeOutAmt = 0.0f;
	private bool isDying = false;
	// Use this for initialization
	void Start () {
		t = Time.time;
		mat = Instantiate(renderer.material) as Material;
		HSBColor c = new HSBColor(Random.Range(0.9f,1.0f),1.0f,Random.Range(0.3f,0.5f));
		mat.SetColor("_Color",c.ToColor());
		renderer.material = mat;
	}

	private float passed
	{
		get { return Time.time - t; }
	}
	
	// Update is called once per frame
	void Update () {
		

		if(passed > life && !isDying)
		{
			isDying = true;
			iTween.ValueTo(gameObject,iTween.Hash("from",0.0f,"to",1.0f,"onupdate","FadeOut","time",0.5f,"oncomplete","KillMe"));
		} 
	}

	void KillMe()
	{
		Destroy(gameObject);
	}

	void FadeOut(float f)
	{
		mat.SetFloat("_SliceAmount",f);
	}
}
