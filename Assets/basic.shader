Shader "BasicDouble"
{
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_SpecColor ("Specular Color", Color) = (1, 1, 1, 1)
	}	
	SubShader {
		Tags {
			"Queue" = "Geometry"
		}
		// First pass - here we render the backfaces of the diamonds. Since those diamonds are more-or-less
		// convex objects, this is effectively rendering the inside of them
		Pass {
			Color (0,0,0,0)
			Offset  -1, -1
			Cull Front
			ZWrite Off
			
		}

	}
}